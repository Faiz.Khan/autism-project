function[Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,Ind]...
   =FVA_analysis(model1,model2,Rxns_interest,~)
% Compares two models using metrics based on FVA
%
% [Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,Ind]...
%   =FVA_analysis(model1,model2,Rxns_interest)
%
% INPUT
% model1             COBRA model structure of 1st model
% model2             COBRA model structure of 2nd model
% Rxns_interest      Reactions of the models to be compared (must exist in both models)
% filenm             File name of Excel sheet generated containing results
% 
% OUTPUT
% Rxns              Reactions of interest ordered by Mean shift
% Mean_shift        Metric - Mean shift of reactions of interest in descending order
% Range_change      Metric - Range change ordered by Mean shift
% model1_min        Min fluxes of reactions of interest in model 1 ordered
%                   by Mean shift
% model1_max        Max fluxes of reactions of interest in model 1 ordered
%                   by Mean shift
% model2_min        Min fluxes of reactions of interest in model 2 ordered
%                   by Mean shift
% model2_max        Max fluxes of reactions of interest in model 2 ordered
%                   by Mean shift
% RxnFormulas       Formulae of rxns of interest ordered by Mean shift
% RxnNames          Names of rxns of interest ordered by Mean shift
% subsyst           Subsytems of rxns of interest ordered by Mean shift
% Ind               Indices of rxns of interest ordered by Mean shift
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019
%% FVA
[model1_min, model1_max]=fluxVariability(model1,100,'max',Rxns_interest);
[model2_min, model2_max]=fluxVariability(model2,100,'max',Rxns_interest);

Mean_shift=abs((model1_min+model1_max)/2 - (model2_min+model2_max)/2);
Range_change=abs(-model1_min+model1_max + model2_min-model2_max );
%% Sort according to mean shift
[Mean_shift,Ind] = sort(Mean_shift,'descend');
Range_change=Range_change(Ind);
model1_min=model1_min(Ind);
model1_max=model1_max(Ind);
model2_min=model2_min(Ind);
model2_max=model2_max(Ind);

Rxns=Rxns_interest(Ind);

% Reaction details
model=model1;
rxn_Ind=findRxnIDs(model,Rxns);
RxnNames=model.rxnNames(rxn_Ind);
RxnFormulas=model.rxnFormulas(rxn_Ind);
subsyst=model.subSystems(rxn_Ind);
end