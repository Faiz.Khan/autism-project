function [FF,R,msg]=LeakTest(Model)
% Checks model for leaks (no net production of any metabolite under no
% input conditions)
%
% [FF,R,msg]=LeakTest(Model)
%
% INPUT
% Model          COBRA model structure of model to be tested
%
% OUTPUT
% FF             FBA min and FBA max values for the reactions defined in the variable R
% R              Exchange Reactions of the model
% msg            Display msg regarding whether model is leaking or not,
%                with leaking reactions
%
% Swagatika Sahoo
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

modelClosed = Model;

exc=strncmpi('EX',modelClosed,length('EX'));
modelexchanges1=find(exc);

exc=strncmpi('DM',modelClosed,length('DM'));
modelexchanges2=find(exc);

exc=strncmpi('sink',modelClosed,length('sink'));
modelexchanges3=find(exc);

modelexchanges=vertcat(modelexchanges1,modelexchanges2,modelexchanges3);

modelClosed.lb(modelexchanges)=0;
modelClosed.ub(modelexchanges)=100;
FF=zeros(length(modelexchanges),2);

for i = 1 : length(modelexchanges)
    modelClosed = changeObjective(modelClosed, modelClosed.rxns(modelexchanges(i)));
    FF1=optimizeCbModel(modelClosed,'min');
    FF2=optimizeCbModel(modelClosed,'max');
    %  FF2=optimizeCbModel(modelClosed,'max',1e-6);
    FF(i,:)=[FF1.f FF2.f];
end
FF(abs(FF)<=1e-8)=0;

R = modelClosed.rxns(modelexchanges);

if(find(FF~=0))
    Leak_rxns=R(FF(:,1)~=0 | FF(:,2)~=0);
    msg=vertcat('Leaking',Leak_rxns);
else
    msg='Not leaking';
end
disp(msg);
end