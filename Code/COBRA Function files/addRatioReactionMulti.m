function modelNew = addRatioReactionMulti(model, listOfRxns, ratioCoeff, csense, b)
%addRatioReactionMulti adds ratio reaction with multiple components. 
%
% modelNew = addRatioReactionMulti(model, listOfRxns, ratioCoeff, csense, b)
%
%INPUTS
% model         COBRA model structure
% listOfRxns    List of Reactions
% ratioCoeff    Array of ratio coefficient between the reactions
%
%OPTIONAL INPUTS 
% csense        Equality/Greater/Lesser than for inequality constraints
% b             The accumulation(s) or release(s) 
%
%OUTPUT
% modelNew      COBRA model structure containing the ratio
%
% Example:      33.3 *ALAtN1_Neuron < (ALATA_Lm_Neuron + ALATA_L_Neuron):
%              modelNew = addRatioReactionMulti(model,{'ALAtN1_Neuron' 'ALATA_Lm_Neuron' 'ALATA_L_Neuron'},[-33.3 1 1],'G');
%
% Ines Thiele 02/09
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

if(nargin<4)
    csense='E';
end

if(nargin<5)
    b=0;
end

[~, Loc] = ismember(listOfRxns,model.rxns);

metID= strcat('Ratio_',strjoin(strcat(listOfRxns,'_')));
modelNew = addMetabolite(model,metID,metID,'','','','','',0,b,csense );

modelNew.S(end,:) = 0;
modelNew.S(end,Loc) = ratioCoeff;

% if isfield(modelNew,'note')
%     modelNew.note = strcat(modelNew.note,listOfRxns{1},' and ',listOfRxns{2}, 'are set to have a ratio of',ratioCoeff1(1),' to ' ,ratioCoeff1(2),'.');
% else
%     modelNew.note = strcat(listOfRxns{1},' and ',listOfRxns{2}, 'are set to have a ratio of ',num2str(ratioCoeff1(1)),':' ,num2str(ratioCoeff1(2)),'.');
% end


