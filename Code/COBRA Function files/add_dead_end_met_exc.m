function [model,List,List_ex]=add_dead_end_met_exc(model,Ext_comp)
% Adds exchanges for dead end metabolites in the external compartment
%
% [model,List,List_ex]=add_dead_end_met_exc(model,Ext_comp)
%
% INPUT
% model             COBRA model structure
%
%OPTIONAL INPUTS
% Ext_comp          External component (default - [e])
%
% OUTPUT
% model             model after adding exchanges for dead end mets
% List              List of dead end metabolites in the model
% List_ex           List of dead end metabolites for which exchanges were
%                   added
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

count=0;
count2=0;

if(nargin<2)
Ext_comp='[e]';
end

List=[];
List_ex=[];
for i=1:length(model.mets)
    if(nnz(model.S(i,:))==1)
        count=count+1;
        List{count}= model.mets{i};
        if(strfind(List{count},Ext_comp))
            count2=count2+1;
            List_ex{count2}=List{count};
        end
    end
end

if(~isempty(List_ex))
    model=addExchangeRxn(model,List_ex);
end
end