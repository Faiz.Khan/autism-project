function cde

% Changes MATLAB current folder to the folder containing the code file
% presently open in the MATLAB editor
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

current_edit = matlab.desktop.editor.getActive();
the_dir = fileparts(current_edit.Filename);
cd(the_dir)
end