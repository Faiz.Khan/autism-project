function model=constr_model(model,diet,ext_cmp,int)
% Constrains model as per gut conditions &/ diet
%
% model=constr_model(model,diet,ext_cmp,int)
%
% INPUT
% model             COBRA model structure
% diet              Diet name ('Western' or 'High_fiber')
%
%OPTIONAL INPUTS
% ext_comp          External component (default - (u))
% int               Case variable = 1 - intestinal conditions (default)
%                                 = any other entry- diet only
%
% OUTPUT
% model             model constrained as per diet &/ intestinal conditions
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

if(nargin<3)
    ext_cmp='(u)';
end

if(nargin<4) % Default - intestinal conditions
    int=1;
end

model=setDiet(model,diet,ext_cmp);

if(int==1)
model=changeRxnBounds(model,'EX_o2(e)',-1,'l');
model=changeRxnBounds(model,'EX_hco3(e)',-1,'l');
model=changeRxnBounds(model,'EX_o2(u)',-1,'l');
model=changeRxnBounds(model,'EX_hco3(u)',-1,'l');
model=changeRxnBounds(model,'EX_dag_hs(u)',-1,'l');
end
% Constraints are imposed after setting diet, as diet changes these
% constraints
end