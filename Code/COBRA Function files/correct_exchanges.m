function model=correct_exchanges(model)
% Renames any incorrectly named exchanges, as per the metabolite names in
% the model, square brackets with parantheses for compartment
%
% model=correct_exchanges(model)
%
% INPUT
% model             COBRA model structure
%
% OUTPUT
% model             model with incorrectly named exchanges renamed
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

% Finding exchange reactions
exc=strncmpi('EX',model.rxns,2);
exch_ind=find(exc);

RxnAbbrev=cell(length(exch_ind),1);
RxnName=cell(length(exch_ind),1);
RxnFormula=cell(length(exch_ind),1);
LB=zeros(length(exch_ind),1);
UB=zeros(length(exch_ind),1);

for i=1:length(exch_ind)
    Met_ind=find(model.S(:,exch_ind(i)));
    RxnAbbrev{i}=strcat('EX_',model.mets{Met_ind});
    
    % Replacing square brackets for compartment with parantheses
    RxnAbbrev{i}=strrep(RxnAbbrev{i},'[e]','(e)');
    RxnAbbrev{i}=strrep(RxnAbbrev{i},'[u]','(u)');
    
    RxnName{i}=strcat(model.metNames{Met_ind},'exchange');
    RxnFormula{i}=strcat(model.mets{Met_ind},' <=> ');
    LB(i)=model.lb(exch_ind(i));
    UB(i)=model.ub(exch_ind(i));
end

model=removeRxns(model,model.rxns(exch_ind));
for i=1:length(exch_ind)
    model = addReaction(model,{RxnAbbrev{i},RxnName{i}},RxnFormula{i},[],1,LB(i),UB(i),0,'Exchange/demand reaction');
end
end