function [Gro_ind,Gro_sum,Gro_pareto]=get_growth(model)
% Finds growth (biomass) of each cell in a multicellular model
%
% [Gro_ind,Gro_sum,Gro_pareto]=get_growth(model)
%
% INPUT
% model             COBRA model structure
%
% OUTPUT
% Gro_ind           Maximal Growth rates of each cell using individual FBA for each cell's biomass
% Gro_sum           Growth rates of each cell using FBA with objective as sum of all cells' biomasses
% Gro_pareto        Growth rates of each cell using Pareto-optimal solution
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

biom_ind=find(model.c);

%% Sum
FBAsol=optimizeCbModel(model);
Gro_sum=FBAsol.x(biom_ind);
%% Pareto growth
Gro_pareto=pareto_optim(model);
%% Individual
Gro_ind=zeros(length(biom_ind),1);
for i=1:length(biom_ind)
    model.c(model.c~=0)=0;
    model.c(biom_ind(i))=1;
    FBAsol=optimizeCbModel(model);
    Gro_ind(i)=FBAsol.f;
end
Gro_ind=Gro_ind';
end