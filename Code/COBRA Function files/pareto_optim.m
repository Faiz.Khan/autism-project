function Gro=pareto_optim(model,Nsteps,biom_rxns,wts,search)
% Finds pareto-optimal solution of the model
%
% Gro=pareto_optim(model,Nsteps,biom_rxns,wts,search)
%
% INPUT
% model             COBRA model structure
%
% OPTIONAL INPUTS
% Nsteps            Number of steps in the search
% biom_rxns         Objective reactions
% wts               Weights in percentages for weighted-pareto-optimization
% search            Search type - 'linear' or 'binary' (default)
%
% OUTPUT
% Gro               Pareto optimal solution
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

if(nargin<2)
    Nsteps=50;
end

if(nargin<3)
    biom_ind=find(model.c);
    biom_rxns=model.rxns(biom_ind);
else
    biom_ind=findRxnIDs(model,biom_rxns);
end

if(nargin<4)
    wts=ones(length(biom_rxns),1)/length(biom_rxns);
end

if(nargin<5)
    search='binary';
end

wts=(1/min(wts))*diag(wts); % Since equal weights => Identity matrix, min wt takes 1 step

model.c(:)=0;
%% Determine bounds

[minFlux,maxFlux]=fluxVariability(model,100,'max',biom_rxns);

model.c(biom_ind)=1;

switch search
    case 'linear'
        %% Finding pareto optimal solution - linear search
        if(length(find(model.c))~=1)
            for Step_no=1:Nsteps
                Flux_Vec=minFlux+wts*Step_no*(maxFlux-minFlux)/Nsteps;
                if(find(Flux_Vec>model.ub(biom_ind))) % Numerical error check
                    break;
                end
                modelNew=changeRxnBounds(model,biom_rxns,Flux_Vec,'l');
                FBAsol=optimizeCbModel(modelNew);
                if(FBAsol.stat==0)
                    break;
                end
            end
            Gro=minFlux+wts*(Step_no-1)*(maxFlux-minFlux)/Nsteps;
        else
            FBAsol=optimizeCbModel(model);
            Gro=FBAsol.f;
        end
        
    case 'binary'
        %%  Finding pareto optimal solution - binary search
        Flux_Vec_old=minFlux;
        if(length(find(model.c))~=1)
            for Step_no=1:Nsteps
                Flux_Vec=minFlux+(wts./(wts+1))*(maxFlux-minFlux);
                if(find(Flux_Vec>model.ub(biom_ind))) % Numerical error check
                    break;
                end
                modelNew=changeRxnBounds(model,biom_rxns,Flux_Vec,'l');
                FBAsol=optimizeCbModel(modelNew);
                if(FBAsol.stat==0) % Infeasible
                    maxFlux=Flux_Vec;
                else
                    minFlux=Flux_Vec;
                    Flux_Vec_old=Flux_Vec;
                end
            end
            Gro=Flux_Vec_old;
        else
            FBAsol=optimizeCbModel(model);
            Gro=FBAsol.f;
        end
end
end