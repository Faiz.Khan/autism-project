function model=rename_model(model,neur)
% Renames brain model neuron (generic) with specific neuron name
%
% model=rename_model(model,neur)
%
% INPUT
% model             COBRA model structure
% neur              Specific neuron
%
% OUTPUT
% model             model renamed as per specific neuron
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

for i=1:length(model.mets)
    model.mets(i)=strrep(model.mets(i),'N]',strcat(neur,']'));
end

for i=1:length(model.rxns)
    model.rxns(i)=strrep(model.rxns(i),'Neuron',neur);
    model.rxns(i)=strrep(model.rxns(i),'N]',strcat(neur,']'));
model.rxnNames(i)=strrep(model.rxnNames(i),'Neuron',neur);
model.rxnNames(i)=strrep(model.rxnNames(i),'N]',strcat(neur,']'));

end

for i=1:length(model.genes)
     model.genes(i)=strcat(model.genes(i),neur);
end

end
