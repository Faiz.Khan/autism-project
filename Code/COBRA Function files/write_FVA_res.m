function write_FVA_res(Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,filenm,Type)
% Writes results of FVA_analysis to Excel spreadsheet and performs
% statistical analysis
%
% write_FVA_res(Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,filenm,Type)
%
% INPUT
% Rxns              Reactions of interest ordered by Mean shift
% Mean_shift        Metric - Mean shift of reactions of interest in descending order
% Range_change      Metric - Range change ordered by Mean shift
% model1_min        Min fluxes of reactions of interest in model 1 ordered
%                   by Mean shift
% model1_max        Max fluxes of reactions of interest in model 1 ordered
%                   by Mean shift
% model2_min        Min fluxes of reactions of interest in model 2 ordered
%                   by Mean shift
% model2_max        Max fluxes of reactions of interest in model 2 ordered
%                   by Mean shift
% RxnFormulas       Formulae of rxns of interest ordered by Mean shift
% RxnNames          Names of rxns of interest ordered by Mean shift
% subsyst           Subsytems of rxns of interest ordered by Mean shift
% filenm            File name of Excel sheet generated containing results
% Type              Additional information to be printed
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

if nargin<12
    Type=[];
end
    
% Writing to sheet
Legend={'Rxns','Rxn names','Formula','min1','max1','min2','max2','Mean shift','Range change','Subsytem'};
xlswrite(filenm,Legend,1,'A1')
xlswrite(filenm,Rxns,1,'A2')
xlswrite(filenm,RxnNames,1,'B2')
xlswrite(filenm,RxnFormulas,1,'C2')
xlswrite(filenm,model1_min,1,'D2')
xlswrite(filenm,model1_max,1,'E2')
xlswrite(filenm,model2_min,1,'F2')
xlswrite(filenm,model2_max,1,'G2')
xlswrite(filenm,Mean_shift,1,'H2')
xlswrite(filenm,Range_change,1,'I2')
xlswrite(filenm,subsyst,1,'J2')
%xlswrite(filenm,Type,1,'K2') % Uncomment for printing Type


Nrxns=length(Rxns);
Nshift_rxns=nnz(Mean_shift>10^(-8));

disp (strcat('Nrxns=',num2str(Nrxns)));
disp (strcat('Nshift_rxns=',num2str(Nshift_rxns)));
%% Statistical analysis
[n,bins]=hist(Mean_shift(Mean_shift>10^(-8)));
bar(bins,n)
% xticks(bins)
% xtickangle(90)
% xtickformat('%.2f')
xlabel('Mean shift (mmol/gDW hr)')
ylabel('No. of rxns')
title('Histogram of mean shifts')
label=strcat('Total rxns =',num2str(Nrxns),', Total shifted rxns =',num2str(Nshift_rxns));
text(bins(round(length(bins)/3)),0.75*Nshift_rxns,label,'EdgeColor','black')
print(strcat(filenm,'_histogram'),'-dpng','-r300')

% Saving
save(filenm);
end