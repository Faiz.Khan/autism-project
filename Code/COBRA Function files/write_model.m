function write_model(model,filenm,rxns_interest)
% Write model to Excel spreadsheet
%
% write_model(model,filenm,rxns_interest)
%
% INPUT
% model             COBRA model structure
% filenm - Name of Excel file
%
% OPTIONAL INPUTS
% rxns_interest - Cell array of rxn names of interest, ex:model.rxns(1:4)
%                 (default - entire model)
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

if(~isfield(model,'rxnFormulas'))
    model.rxnFormulas=printRxnFormula(model,model.rxns,false,true,false);
    model.rxnFormulas2=printRxnFormula(model,model.rxns,false,true,true);
end

sheet1='Rxns';

if(nargin<3)
    %% Writing entire model to sheet
    
    sheet2='Mets';
    
    xlswrite(filenm,{'Sub systems', 'Rxn names', 'Rxn IDs','Formula','LB','UB','Detailed Formula'},sheet1,'A1');
    xlswrite(filenm,model.subSystems,sheet1,'A2');
    xlswrite(filenm,model.rxnNames,sheet1,'B2');
    xlswrite(filenm,model.rxns,sheet1,'C2');
    if isfield(model,'rxnFormulas')
        xlswrite(filenm,model.rxnFormulas,sheet1,'D2');
    end
    xlswrite(filenm,model.lb,sheet1,'E2');
    xlswrite(filenm,model.ub,sheet1,'F2');
    if isfield(model,'rxnFormulas2')
        xlswrite(filenm,model.rxnFormulas2,sheet1,'G2');
    end
    
    xlswrite(filenm,{'Met names', 'Met IDs', 'Met formulas', 'Met Charge'},sheet2,'A1');
    xlswrite(filenm,model.metNames,sheet2,'A2');
    xlswrite(filenm,model.mets,sheet2,'B2');
    xlswrite(filenm,model.metFormulas,sheet2,'C2');
   % xlswrite(filenm,model.metCharge,sheet2,'D2');
else
    %% Writing only reactions of interest
    RxnIDs=findRxnIDs(model,rxns_interest);
    
    xlswrite(filenm,{'Sub systems', 'Rxn names', 'Rxn IDs','Formula','LB','UB'},sheet1,'A1');
    xlswrite(filenm,model.subSystems(RxnIDs),sheet1,'A2');
    xlswrite(filenm,model.rxnNames(RxnIDs),sheet1,'B2');
    xlswrite(filenm,model.rxns(RxnIDs),sheet1,'C2');
    xlswrite(filenm,model.rxnFormulas(RxnIDs),sheet1,'D2');
    xlswrite(filenm,model.lb(RxnIDs),sheet1,'E2');
    xlswrite(filenm,model.ub(RxnIDs),sheet1,'F2');
    xlswrite(filenm,model.rxnFormulas2(RxnIDs),sheet1,'G2');
    
end
end