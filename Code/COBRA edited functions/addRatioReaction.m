function modelNew = addRatioReaction(model, listOfRxns, ratioCoeff, csense, b)
%addRatioReaction adds ratio reaction. 
%
% modelNew = addRatioReaction(model, listOfRxns, ratioCoeff)
%
%INPUTS
% model         COBRA model structure
% listOfRxns    List of 2 Reactions
% ratioCoeff    Array of ratio coefficient between the 2 reactions
%
%OPTIONAL INPUTS 
% csense        Equality/Greater/Lesser than for inequality constraints
% b             The accumulation(s) or release(s) 
%OUTPUT
% modelNew      COBRA model structure containing the ratio
%
% Example:      1 v_EX_ac(e) = 2 v_EX_for(e):
%               modelNew = addRatioReaction(model, {'EX_ac(e)' 'EX_for(e)'}, [1 2])
%
%
% Ines Thiele 02/09
% % Meghana V Palukuri and Faiz K. Mohammed 13/06/2019 (Added inequality constraint functionalities)


if(nargin<4)
    csense='E';
end

if(nargin<5)
    b=0;
end


[~, Loc] = ismember(listOfRxns,model.rxns);

metID= strcat('Ratio_',listOfRxns{1},'_',listOfRxns{2});
modelNew = addMetabolite(model,metID,metID,'','','','','',0,b,csense );

modelNew.S(end,:) = 0;
modelNew.S(end,Loc) = [-ratioCoeff(1) ratioCoeff(2)];

if isfield(modelNew,'note')
    modelNew.note = strcat(modelNew.note,listOfRxns{1},' and ',listOfRxns{2}, 'are set to have a ratio of',ratioCoeff(1),' to ' ,ratioCoeff(2),'.');
else
    modelNew.note = strcat(listOfRxns{1},' and ',listOfRxns{2}, 'are set to have a ratio of ',num2str(ratioCoeff(1)),':' ,num2str(ratioCoeff(2)),'.');
end


