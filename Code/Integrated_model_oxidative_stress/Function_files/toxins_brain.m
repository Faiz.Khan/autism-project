function [Neuro_prod,ATP_pareto,Toxin_consum_brain,Toxin_prod_brain]=toxins_brain(model,Toxin_flux)
% Calculates toxin production & consumption in the brain
%
% [Neuro_prod,ATP_pareto,Toxin_consum_brain,Toxin_prod_brain]=toxins_brain(model,Toxin_flux)
%
% INPUT
% model             COBRA model structure
% Toxin_flux        Max possible toxin input flux bound
%
% OUTPUT
% Neuro_prod            Neurotransmitter production fluxes
% ATP_pareto            ATP maintenance fluxes
% Toxin_prod_brain      Toxin production flux         
% Toxin_consum_brain    Toxin consumption flux
%
% Meghana V Palukuri 7/7/2017

% Reactions of interest
Neuro_rxns={'DM_4abut[cGabaN]','DM_glu_L[cGluN]'};
Toxin_in_ex={'h2o2_input','o2s_input'};

% Set max toxin uptake
model=changeRxnBounds(model,Toxin_in_ex,Toxin_flux,'u');

% Find ATP maintenance
ATP_pareto=pareto_optim(model,10);
model.lb(model.c~=0)=0.99999*ATP_pareto;
model.c(:)=0;

% Find & set max toxin consumption
Toxin_consum_brain=pareto_optim(model,10,Toxin_in_ex);
model=changeRxnBounds(model,Toxin_in_ex,0.99999*Toxin_consum_brain,'l'); 
Toxin_consum_brain=Toxin_consum_brain';

% Find & set max toxin production
Toxin_out_ex={'h2o2_output','o2s_output'};
Toxin_prod_brain=pareto_optim(model,10,Toxin_out_ex);
model=changeRxnBounds(model,Toxin_out_ex,0.99999*Toxin_prod_brain,'l');
Toxin_prod_brain=Toxin_prod_brain';

% Neurotransmitter production
%[~,Neuro_prod]=fluxVariability(model,100,'max',Neuro_rxns);
Neuro_prod=pareto_optim(model,10,Neuro_rxns);
Neuro_prod=Neuro_prod';
end