function [ToxinMaxProd,ToxinMaxConsum,Gro]=toxins_gut(model,Toxin_input_gut,ben_per)
% Calculates toxin production & consumption in the gut
%
% [ToxinMaxProd,ToxinMaxConsum,Gro]=toxins_gut(model,Toxin_input_gut,ben_per)
%
% INPUT
% model             COBRA model structure
% Toxin_input_gut   Max possible toxin input flux bound
% ben_per           Beneficial bacteria percentage
%
% OUTPUT
% ToxinMaxProd      Toxin production flux         
% ToxinMaxConsum    Toxin consumption flux
% Gro               Biomass of each cell (pareto-optimal)
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019

% Reactions of interest
Toxin_in_ex={'h2o2_input','o2s_input'};

% Setting Max possible toxin input bound
model=changeRxnBounds(model,Toxin_in_ex,Toxin_input_gut,'u'); 

N_cells=length(find(model.c));

if(ben_per~=0)
harm_per=1-ben_per; % Harmful bacteria 
% Weightage - SIEC = 1/6, Remn 5/6 = Bacteria, Each Harmful bacteria has
% equal weightage, Each beneficial bacteria has equal weightage
% wts=[5/6*(harm_per/2);5/6*(ben_per/3);
%     5/6*(harm_per/2);5/6*(ben_per/3);
%     5/6*(ben_per/3);1/6]; %for five microbes 
% wts=[8/9*(harm_per/5);8/9*(ben_per/3);8/9*(harm_per/5);
%     8/9*(ben_per/3);9/10*(harm_per/5);8/9*(ben_per/3);8/9*(harm_per/5);8/9*(harm_per/5);1/9]; %for nine microbes
wts=[10/11*(harm_per/5);10/11*(ben_per/5);
    10/11*(harm_per/5);10/11*(ben_per/5);
    10/11*(ben_per/5);10/11*(ben_per/5);
    10/11*(harm_per/5);10/11*(ben_per/5);
    10/11*(harm_per/5);10/11*(harm_per/5);1/11]; %for ten microbes
else
    wts = 1/N_cells*ones(N_cells,1); % Equal
end

% Find & set Max growth under these conditions
Gro(1:N_cells)=pareto_optim(model,10,model.rxns(model.c~=0),wts);
model.lb(model.c~=0)=Gro(1:N_cells); 
model.c(:)=0;

% Find & set max. toxin production
Toxin_out_ex={'h2o2_output','o2s_output'};
ToxinMaxProd=pareto_optim(model,10,Toxin_out_ex);
model=changeRxnBounds(model,Toxin_out_ex,ToxinMaxProd,'l'); 
ToxinMaxProd=ToxinMaxProd';

% Find max consumption under max. prod
ToxinMaxConsum=pareto_optim(model,10,Toxin_in_ex);
ToxinMaxConsum=ToxinMaxConsum';
end