% Integrated COBRA-PBPK model implemented for ROS
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019
clc
clearvars
close all
cde
tic;

%% Initial condns
init_H2O2 = [0 0 0 0 0 0 0 0 0 0.0001 0 0];
init_SOX = [0 0 0 0 0 0 0 0 0 0.0001 0 0];

fin_conc=vertcat(init_H2O2,init_SOX); % Initializing PKPK model output concentrations (all organs)
PBPK_output=[init_H2O2(10),init_SOX(10);...
    init_H2O2(2),init_SOX(2)]; % Initializing PKPK model outputs for gut & brain

tend=6; % Total time of simulation (hrs)
N_hrs=0.25; % Steo size (hrs)
t=0:N_hrs:tend;
t=t';
%% Load brain model
load Brain_combined
model_brain=model;
clear model;

%% Load gut model

% Uncomment required models of analysis

% Name={'SIEC_Western';....
%    'SIEC_High_fiber'};

% Name={'Gut_Microbiome_Western'};
Name={'Gut_Microbiome_Beneficial_High_fiber'};

% Name={'Gut_Microbiome_Western';....
%     'Gut_Microbiome_High_fiber'};

% Name={'Gut_Microbiome_Western';....
%     'Gut_Microbiome_High_fiber';...
%     'Gut_Microbiome_Harmful_Western';....
%     'Gut_Microbiome_Harmful_High_fiber';...
%     'Gut_Microbiome_Beneficial_Western';....
%     'Gut_Microbiome_Beneficial_High_fiber'};

Name={'Gut_Microbiome_Western1';....
    'Gut_Microbiome_High_fiber1';...
    'Gut_Microbiome_Harmful_Western1';....
    'Gut_Microbiome_Harmful_High_fiber1';...
    'Gut_Microbiome_Beneficial_Western1';....
    'Gut_Microbiome_Beneficial_High_fiber1';...
    'SIEC_Western1';....
    'SIEC_High_fiber1'};

% Making folder for results
mkdir('Results');
cd('Results');

% ben_per_all=[0.2 0.4 0.6 0.8]; % All beneficial bacteria % to be tested
% ben_per_all=0; % Default
ben_per_all=1;

%% Integrated model simulation
for index=1:length(Name) % Looping over gut models being simulated
    for ind2=1:length(ben_per_all) % Looping over beneficial bacteria % being simulated
        ben_per=ben_per_all(ind2);
        name= Name{index};
        load(name);
        if(exist('model_diet','var'))
            model_gut=model_diet;
        else
            model_gut=model;
        end
        clear model_diet;
        
        Toxin_lumen_ex={'EX_h2o2(u)','EX_o2s(u)'};
        model_gut=changeRxnBounds(model_gut,Toxin_lumen_ex,zeros(1,length(Toxin_lumen_ex)),'l');
        
        %% Simulate
        
        % Initializing variables
        N_org=length(init_SOX); % No. of organs
        N_toxins=2;
        Conc_H2O2=zeros(tend,N_org);
        Conc_SOX=zeros(tend,N_org);
        Conc_Neuro=zeros(tend+1,2);
        Neuro_prod=zeros(tend,2); % Neurotransmitter production fluxes
        
        % Gut variables
        Toxin_conc_gut=zeros(tend,N_toxins);
        Toxin_input_gut=zeros(tend,N_toxins); % Toxin input flux obtained from concentration
        Toxin_in_gut_limit=zeros(tend,N_toxins); % Limit on toxin input flux based on toxin flux input in previous time step 
        Toxin_input_gut_final=zeros(tend,N_toxins); % Toxin input flux set as bound
        Toxin_prod_gut=zeros(tend,N_toxins); % Toxin production flux from gut
        Toxin_consum_gut=zeros(tend,N_toxins); % Toxin consumption flux in gut
        Gro=zeros(tend,length(find(model_gut.c))); % Biomass of each cell (pareto optimal) in gut
        
        % Corresponding variables for brain
        Toxin_conc_brain=zeros(tend,N_toxins);
        Toxin_input_brain=zeros(tend,N_toxins);
        Toxin_in_brain_limit=zeros(tend,N_toxins);
        Toxin_input_brain_final=zeros(tend,N_toxins);
        Toxin_consum_brain=zeros(tend,N_toxins);
        ATP_pareto=zeros(tend,length(find(model_brain.c)));
        Toxin_prod_brain=zeros(tend,N_toxins);

        % Max Percentage change in flux allowed from one time step to the
        % next
        %per_cent=0.4;
        per_cent=0.2;
        %per_cent=0.9;
        
        for i=1:(length(t)-1) % Looping over time steps
            disp('Outer loop')
            disp(i)
            %% Gut model
            Toxin_conc_gut(i,:)=PBPK_output(1,:);
            
            % Assuming entire concentration can be consumed in one time step -
            % here one hour, if assumed to be possible to consume in a day -
            % then divide by 24
            %converting toxin conc in gut in mols/l to flux
            %(mmols/g.dry.wt.tissue*hr)
            %flux = conc * volume of extravascular region of tissue in
            %rat/g.dry.wt of organ in rat
            %volume of extravascular region of gut in rat = 7.8 ml
            %g.dry.wt of gut in rat = 7.26 g
            Toxin_input_gut(i,:)=Toxin_conc_gut(i,:)*10^(3)*((7.8/1.1)*10^-3)/(7.26);
            
            if(i==1) % At t0
                Toxin_in_gut_limit(i,:)=Toxin_input_gut(i,:);
            else % Setting limit on toxin input flux based on prev time step
                Toxin_in_gut_limit(i,:)=(1+per_cent)*Toxin_input_gut_final(i-1,:);
                for p=1:N_toxins
                    if(Toxin_in_gut_limit(i,p)==0) % If prev time step input flux was 0
                        Toxin_in_gut_limit(i,p)=Inf;
                    end
                end
            end
            
            Toxin_input_gut_final(i,:)=min(Toxin_input_gut(i,:),Toxin_in_gut_limit(i,:));
            
            % Find toxin production & consumption fluxes in gut
            [Toxin_prod_gut(i,:),Toxin_consum_gut(i,:),Gro(i,:)]=toxins_gut(model_gut,Toxin_input_gut_final(i,:),ben_per);
            %% Brain model
            Toxin_conc_brain(i,:)=PBPK_output(2,:);
            
            %converting toxin conc in gut in mols/l to flux
            %(mmols/g.dry.wt.tissue*hr)
            %volume of extravascular region of brain in rat = 0.5989 ml
            %g.dry.wt of gut in rat = 2 g
            Toxin_input_brain(i,:)=Toxin_conc_brain(i,:)*10^(3)*((5.989/1.1)*10^-4)/(2);
            
            if(i==1)
                Toxin_in_brain_limit(i,:)=Toxin_input_brain(i,:);
            else
                Toxin_in_brain_limit(i,:)=(1+per_cent)*Toxin_input_brain_final(i-1,:);
                for p=1:N_toxins
                    if(Toxin_in_brain_limit(i,p)==0)
                        Toxin_in_brain_limit(i,p)=Inf;
                    end
                end
            end
            Toxin_input_brain_final(i,:)=min(Toxin_input_brain(i,:),Toxin_in_brain_limit(i,:));
            
            % Finding maximal toxin production & consumption in brain
            [Neuro_prod(i,:),ATP_pareto(i,:),Toxin_consum_brain(i,:),Toxin_prod_brain(i,:)]=toxins_brain(model_brain,Toxin_input_brain_final(i,:));
            
            % Set actual toxin fluxes to be 1% of maximum values
            Toxin_prod_brain(i,1)=0.001*Toxin_prod_brain(i,1); 
            Toxin_consum_brain(i,1)=0.001*Toxin_consum_brain(i,1);
            %% Transport
            
            % PBPK model simulation to get concentrations, with fluxes as
            % input
            [PBPK_output, fin_conc]  = integrated_PBPK_main(Toxin_prod_gut(i,:), Toxin_prod_brain(i,:), Toxin_consum_gut(i,:), Toxin_consum_brain(i,:), fin_conc,N_hrs);
            
            % Ensure no negative concentrations
            PBPK_output(PBPK_output<0)=0;
            fin_conc(fin_conc<0)=0;
            
            % Store concentrations of toxins
            Conc_H2O2(i,:)=fin_conc(1,:);
            Conc_SOX(i,:)=fin_conc(2,:);
        end % End of simulation over all time steps
        
        Conc_H2O2=vertcat(init_H2O2,Conc_H2O2);
        Conc_SOX=vertcat(init_SOX,Conc_SOX);
        
        % Calculating neurotransmitter concentrations
        for j=2:length(t)
            Conc_Neuro(j,:)=(Conc_Neuro(1,:)*(5.989/1.1)+(0.7*1407*1)*sum(Neuro_prod(1:(j-1))))/(5.989/1.1);
        end
        %% Plot profiles
        if(length(ben_per_all)==1)
            mkdir(Name{index})
            cd(Name{index})
        else
            mkdir(strcat('Ben_per=',num2str(ben_per)))
            cd(strcat('Ben_per=',num2str(ben_per)))
        end
        
        % Gut
        
        figure;
        plot(t,Conc_H2O2(:,10),'k-','LineWidth',1);
        title('H2O2 - gut');
        xlabel('Time (hours)');
        ylabel('Concentration (M)');
        axis tight
        set(gca,'FontSize',12)
        set(findall(gcf,'type','text'),'FontSize',12)
        print('H2O2_gut','-dpng','-r300');
        
        figure;
        plot(t,Conc_SOX(:,10),'k-','LineWidth',1);
        title('SOX - gut');
        xlabel('Time (hours)');
        ylabel('Concentration (M)');
        axis tight
        set(gca,'FontSize',12)
        set(findall(gcf,'type','text'),'FontSize',12)
        print('SOX_gut','-dpng','-r300');
        
        % Brain
        
        figure;
        plot(t,Conc_H2O2(:,2),'k-','LineWidth',1);
        title('H2O2 - brain');
        xlabel('Time (hours)');
        ylabel('Concentration (M)');
        axis tight
        set(gca,'FontSize',12)
        set(findall(gcf,'type','text'),'FontSize',12)
        print('H2O2_brain','-dpng','-r300');
        
        figure;
        plot(t,smooth(Conc_SOX(:,2)),'k-','LineWidth',1);
        title('SOX - brain');
        xlabel('Time (hours)');
        ylabel('Concentration (M)');
        axis tight
        set(gca,'FontSize',12)
        set(findall(gcf,'type','text'),'FontSize',12)
        print('SOX_brain','-dpng','-r300');
        
        % Neurotransmitters
        figure;
        plot(t,Conc_Neuro(:,1),'k-','LineWidth',1);
        title('GABA - brain');
        xlabel('Time (hours)');
        ylabel('Concentration (M)');
        axis tight
        set(gca,'FontSize',12)
        set(findall(gcf,'type','text'),'FontSize',12)
        print('GABA_brain','-dpng','-r300');
        
        figure;
        plot(t,Conc_Neuro(:,1),'k-','LineWidth',1);
        title('Glutamate - brain');
        xlabel('Time (hours)');
        ylabel('Concentration (M)');
        axis tight
        set(gca,'FontSize',12)
        set(findall(gcf,'type','text'),'FontSize',12)
        print('Glutamate_brain','-dpng','-r300');
        %% Important parameters
        % SS -> Steady State
        SS_H2O2_conc_brain=Conc_H2O2(end,2);
        SS_H2O2_conc_gut=Conc_H2O2(end,10);
        
        SS_SOX_conc_brain=Conc_SOX(end,2);
        SS_SOX_conc_gut=Conc_SOX(end,10);
        
        SS_GABA_conc_brain=Conc_Neuro(end,1);
        
        SS_Glutamate_conc_brain=Conc_Neuro(end,2);
        
        SS_conc=[SS_H2O2_conc_brain,SS_H2O2_conc_gut,SS_SOX_conc_brain,SS_SOX_conc_gut,...
            SS_GABA_conc_brain,SS_Glutamate_conc_brain];
        
        % Fluxes
        Toxin_net_brain=Toxin_prod_brain-Toxin_consum_brain;
        Toxin_net_gut=Toxin_prod_gut-Toxin_consum_gut;
        
        SS_flux=[];
        for i=1:N_toxins
            SS_flux=horzcat(SS_flux,[Toxin_prod_brain(end,i),Toxin_consum_brain(end,i),Toxin_net_brain(end,i),Toxin_prod_gut(end,i),Toxin_consum_gut(end,i),Toxin_net_gut(end,i)]);
        end
        SS_flux=horzcat(SS_flux,Neuro_prod(end,:));
        if(length(ben_per_all)==1)
            SS_flux_all(index,:)=SS_flux;
            SS_conc_all(index,:)=SS_conc;
        else
            SS_flux_all(ind2,:)=SS_flux;
            SS_conc_all(ind2,:)=SS_conc;
        end
        %% Saving
        save('All_vars');
        cd('..')
    end
end
%% Writing important parameters to sheet

inp={'Prod','Consum','Net(Prod-Consum)'};
inp=repmat(inp,1,2*N_toxins);

Legend_col={'H2O2_brain','H2O2_gut','SOX_brain','SOX_gut',...
    'GABA','Glutamate'};

Legend_col2={'H2O2_brain','','','H2O2_gut','','','SOX_brain','','','SOX_gut','','',...
    'GABA','Glutamate'};
fnm='SS_toxin_neuro_vals';
if(length(ben_per_all)==1)
    xlswrite(fnm,Name,'SS_Conc','A2');
else
    xlswrite(fnm,ben_per_all','SS_Conc','A2');
end
xlswrite(fnm,Legend_col,'SS_Conc','B1');
xlswrite(fnm,SS_conc_all,'SS_Conc','B2');

if(length(ben_per_all)==1)
    xlswrite(fnm,Name,'SS_Flux','A3');
else
    xlswrite(fnm,ben_per_all','SS_Flux','A3');
end
xlswrite(fnm,Legend_col2,'SS_Flux','B1');
xlswrite(fnm,inp,'SS_Flux','B2');
xlswrite(fnm,SS_flux_all,'SS_Flux','B3');
toc;
%% Saving toxin fluxes for use in model analysis with FVA
Toxin_ex_flux_gut_all=[SS_flux_all(:,5),SS_flux_all(:,4),SS_flux_all(:,11),SS_flux_all(:,10)];
Toxin_ex_flux_brain_all=[SS_flux_all(:,2),SS_flux_all(:,1),SS_flux_all(:,8),SS_flux_all(:,7)];
save('Toxin_ex_fluxes','Toxin_ex_flux_gut_all','Toxin_ex_flux_brain_all');