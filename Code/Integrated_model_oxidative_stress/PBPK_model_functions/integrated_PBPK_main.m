% FUNCTION: integrated_PBPK_main.m
% ______________________________
% DESCRIPTION: Function to integrate PBPK model with the bacteria-sIEC model for a 500g rat. Units : [L M T] = [dm g s]. 
% INPUTS:
% 	array[] gen_gut: Array of toxin generation values (PA, peroxide and superoxide in that order) in the gut by the bacteria-sIEC model at the given time instant (measured in hours). 
%	array[] gen_brain: Array of toxin generation values (PA, peroxide and superoxide in that order) in the brain by the neuronal model at the given time instant (measured in hours). 
% 	array[] cons_gut: Array of toxin consumption values (PA, peroxide and superoxide in that order) in the gut by the bacteria-sIEC model at the given time instant (measured in hours). 
%	array[] cons_brain: Array of toxin consumption values (PA, peroxide and superoxide in that order) in the brain by the neuronal model at the given time instant (measured in hours). 
%	array[][] init: Matrix of concentration values in the six tissue types (brain, heart, adipose, liver, gut and kidney in that order arranged column-wise) for each of the toxins (PA, peroxide and superoxide in that order arranged row-wise).
% OUTPUTS: 
%	Matrix of final concentrations of each of the toxins (PA, peroxide and superoxide in that order arranged column-wise) in the gut and the brain in that order arranged row-wise (2*3 matrix) concatenated with the final concentrations of the toxins in each of the tissue types in the order as mentioned above (3*6 matrix). 



function [c, fin_conc]  = integrated_PBPK_main(gen_gut, gen_brain, cons_gut, cons_brain, init,N_hrs)

% Order of tissue types = [brain, heart, adipose, liver, gut, kidney]

V = [5.989*10^-4 3*10^-3 1.9*10^-2 1.192*10^-2 7.8*10^-3 1.58*10^-3]; %volume of tissue type in the order mentioned above 
Q = [0.016 0.083 0.008 0.021 0.042 0.016]/60; %flowrate of blood through each tissue type
%K_PA = [1.703 5.269 7.993 12.655 10.138 9.739]; %partition coefficient of each tissue type for propionic acid
K_peroxide = [3.056 4.935 8.963 12.243 10.075 8.964]; %partition coefficient of each tissue type for peroxide
K_superoxide = [2.794 4.915 10.016 5.301 5.167 4.718]; %partition coefficient of each tissue type for superoxide
S = [6*10^-2 1.005*10^-1 3.443*10^-1 2.523*10^-1 2.46 8.264*10^-2]; %surface area of cross-section for each tisse type

tspan=0:100;
%tspan = [0 100];
%init_PA = init(1,:);
init_peroxide = init(1,:);
init_superoxide = init(2,:);

% The assumption is that the permeability of the tissue type is constant for 'tspan' seconds after which it is updated as per the final concentrations of the toxins in the gut. The new permeability is used for the next 'tspan' seconds. This continues for one time step i.e. 1 hour.
tstep=100; % 100s default
Mega_tstep=3600*N_hrs;
for t=1:tstep:Mega_tstep 
    disp('Time instant in PBPK model:');
    disp(t);
    P_gut = permeability([init_peroxide(10) init_superoxide(10)]); %computing permeability in gut
    %[~, c_PA] = ode45(@(t, c) model_PA(t, c, Q, V, K_PA, S, gen_gut(1), gen_brain(1), cons_gut(1), cons_brain(1), P_gut), tspan, init_PA);
    [~, c_peroxide] = ode45(@(t, c) model_peroxide(t, c, Q, V, K_peroxide, S, gen_gut(1), gen_brain(1), cons_gut(1), cons_brain(1), P_gut), tspan, init_peroxide); 
    [~, c_superoxide] = ode45(@(t, c) model_superoxide(t, c, Q, V, K_superoxide, S, gen_gut(2), gen_brain(2), cons_gut(2), cons_brain(2), P_gut), tspan, init_superoxide);
    %init_PA = c_PA(end,:);
    init_peroxide = c_peroxide(end,:);
    init_superoxide = c_superoxide(end,:); %updating initial concentration of toxins for next iteration
end

c = [c_peroxide(end,10) c_superoxide(end,10); c_peroxide(end,2) c_superoxide(end,2)]; %final concentrations of each of the toxins (PA, peroxide and superoxide in that order arranged column-wise) in the gut and the brain in that order arranged row-wise (2*3 matrix)
fin_conc = [c_peroxide(end,:); c_superoxide(end,:)]; %final concentrations of the toxins in each of the tissue types in the order as mentioned above (3*6 matrix)
