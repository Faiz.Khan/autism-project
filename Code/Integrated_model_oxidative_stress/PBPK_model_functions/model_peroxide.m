% FUNCTION: model_peroxide.m
% ______________________________
% DESCRIPTION: Function passed to ode45 in integrate_PBPK_main.m to be integrated. Function generates the concentration of peroxide in each tissue type (brain, heart, adipose, liver, gut and kidney)
% INPUTS: (Order of tissue types = [brain, heart, adipose, liver, gut, kidney])
% 	array[] Q: Array of flowrate of blood through each tissue type
%	array[] V: Array of volume of each tissue type
%	array[] K: Array of partition coefficient of peroxide in each tissue type
%	array[] S: Array of surface area of cross-section of each tisse type
%	float gen_gut: peroxide generation value in the gut by the bacteria-sIEC model at the given time step (measured in hours).  
%	float gen_brain: peroxide generation value in the brain by the neuronal model at the given time instant (measured in hours). 
% 	float cons_gut: peroxide consumption value in the gut by the bacteria-sIEC model at the given time instant (measured in hours). 
%	float cons_brain: peroxide consumption values in the brain by the neuronal model at the given time instant (measured in hours). 
%	float P_gut: permeability of peroxide in the gut (and the brain).


function dcdt = model_peroxide(t, c, Q, V, K, S, gen_gut, gen_brain, cons_gut, cons_brain, P_gut)
dcdt = zeros(12,1);
f = 0.1; % tissue fractional blood volume
D = 10^(-10); %diffusion coefficient for the diffusion of the compound across the cell lipid wall
delx = 8 * 10^(-8); %thickness of the cell membrane

P_brain = P_gut;

% Refer to set of PBPK equations in report
%Brain
dcdt(1) = ((1+f)/(f*V(1))) * ( (Q(1)*(c(3) - c(1))) + (P_brain*S(1)*c(2)/K(1)) - (P_brain*S(1)*c(1)) );
dcdt(2) = ((1+f)/V(1)) * ( (P_brain*S(1)*c(1)) - (P_brain*S(1)*c(2)/K(1)) - (2*cons_brain*10^-3/3600) + (2*gen_brain*10^-3/3600) );

%Heart
c2in = (c(1)*Q(1) + c(5)*Q(3) + c(7)*Q(4) + c(9)*Q(5) + c(11)*Q(6))/Q(2); % Concentration of peroxide in venal blood stream
dcdt(3) = ((1+f)/(f*V(2))) * ( (Q(2)*(c2in - c(3))) + ((K(2)*D/delx)*S(2)*c(4)/K(2)) - ((K(2)*D/delx)*S(2)*c(3)) );
dcdt(4) = ((1+f)/V(2)) * ( ((K(2)*D/delx)*S(2)*c(3)) - ((K(2)*D/delx)*S(2)*c(4)/K(2)) );

%Adipose
dcdt(5) = ((1+f)/(f*V(3))) * ( (Q(3)*(c(3) - c(5))) + ((K(3)*D/delx)*S(3)*c(6)/K(3)) - ((K(3)*D/delx)*S(3)*c(5)) );
dcdt(6) = ((1+f)/V(3)) * ( ((K(3)*D/delx)*S(3)*c(5)) - ((K(3)*D/delx)*S(3)*c(6)/K(3)) );

%Liver
dcdt(7) = ((1+f)/(f*V(4))) * ( (Q(4)*(c(3) - c(7))) + ((K(4)*D/delx)*S(4)*c(8)/K(4)) - ((K(4)*D/delx)*S(4)*c(7)) );
dcdt(8) = ((1+f)/V(4)) * ( ((K(4)*D/delx)*S(4)*c(7)) - ((K(4)*D/delx)*S(4)*c(8)/K(4)) - (6.36*10^-6*V(4)*c(8)/(c(8) + (0.85*10^-3))) - (0.001375*V(4)*c(8)/(c(8) + (3*10^-3))) - (2.07*10^-6*V(4)*c(8)/(c(8) + (3.38*10^-6))) - (1.88*10^-5*V(4)*c(8)/(c(8) + (3.34*10^-3))) - (100.8*10^-6*V(4)*c(8)/(c(8) + (18*10^-6))) );

%Gut
dcdt(9) = ((1+f)/(f*V(5))) * ( (Q(5)*(c(3) - c(9))) + ((P_gut)*S(5)*c(10)/K(5)) - ((P_gut)*S(5)*c(9)) );
dcdt(10) = ((1+f)/V(5)) * ( ((P_gut)*S(5)*c(9)) - ((P_gut)*S(5)*c(10)/K(5)) + (7.26*gen_gut*10^-3/3600) - (7.26*cons_gut*10^-3/3600) );

%Kidney
dcdt(11) = ((1+f)/(f*V(6))) * ( (Q(6)*(c(3) - c(11))) + ((K(6)*D/delx)*S(6)*c(12)/K(6)) - ((K(6)*D/delx)*S(6)*c(11)) );
dcdt(12) = ((1+f)/V(6)) * ( ((K(6)*D/delx)*S(6)*c(11)) - ((K(6)*D/delx)*S(6)*c(12)/K(6)) - (10.24*10^-3*0.5*c(12)/60) );
