% FUNCTION: permeability.m
% ______________________________
% DESCRIPTION: Function to compute the permeability of the gut using the equation proposed by Watson et. al. We have assumed that the brain and the gut have the same permeability.
% INPUTS:
% 	array[] c: Array of toxin concentrations of PA, peroxide and superoxide in that order in the gut
% OUTPUTS: 
%	float P_gut: permeability of the gut (and the brain).


function P_gut = permeability(c)
C_max = 110.6 * 10^-9/17500; % maximum concentration of IL-1beta (molecular mass 17500 u) cytokine produced in the body in autistic cases (mol/l)
K_c = 0.12 * 10^-9/17500; % rapidity of immune response (mol/l)
C_cyto = C_max * sum(c)/(K_c + sum(c)); %concentration of IL-1beta produced according to Michaelis-Menten kinetics
r_smin = 4.5*10^-9; %tight junction pore size in the absence of any inflammatory cytokines (dm)
r_smax = 7.55*10^-9; %maximum pore size the tight junction can have when exposed to high concentrations of cytokines (dm)
K_m = 10^-15; %magnitude of the effect of the cytokine on the tight junction pore size (mol/l)
r_s = r_smin + (r_smax - r_smin)*(C_cyto/(K_m + C_cyto)); 
r = 3.1 * 10^-9; %epithelial pore radius
beta = 3.9 * 10^-11;
alpha = 1.53*10^6;
P_gut = (alpha * (r_s*r_s/r) * (1 - (r/r_s))^2) + (beta/r);
