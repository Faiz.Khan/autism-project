% Finding bacterial secretion products
% Meghana V Palukuri 7/7/2017

clc
clearvars
close all
cde

mkdir('Results');
cd('Results');
%% Classifying common SIEC exchanges between bacteria

% Loading model
load Gut_microbiome_Western.mat

% Finding SIEC lumen exchanges
exc=regexp(model_diet.rxns(12140:end),{'\(u\)'}); 
exch_SIEC_ind=12139+find(~cellfun(@isempty,exc)); % Indices of the exchanges

Nbact=zeros(length(exch_SIEC_ind),1); % No. of bacteria secreting the metabolite

% Bacteria shortforms 
BV=[];
DD=[];
CP=[];
LA=[];
BL=[];
AKK=[];
CLS=[];
PRE=[];
RT=[];
SH=[];
for i=1:length(exch_SIEC_ind)
    MetID=find(model_diet.S(:,exch_SIEC_ind(i))); % Getting the exchange met
    Rxn_IDs=(find(model_diet.S(MetID,1:5699))); % Finding reactions of this Metabolite in the bacteria
    for j=1:length(Rxn_IDs)
        if(Rxn_IDs(j)<1314)
            BV=vertcat(BV,model_diet.rxns(exch_SIEC_ind(i)));
        elseif(Rxn_IDs(j)<2593)
            DD=vertcat(DD,model_diet.rxns(exch_SIEC_ind(i)));
        elseif(Rxn_IDs(j)<3911)
            CP=vertcat(CP,model_diet.rxns(exch_SIEC_ind(i)));
        elseif(Rxn_IDs(j)<4683)
            LA=vertcat(LA,model_diet.rxns(exch_SIEC_ind(i)));
        elseif(Rxn_IDs(j)<5748)
            BL=vertcat(BL,model_diet.rxns(exch_SIEC_ind(i)));
        elseif(Rxn_IDs(j)<6873)
            AKK=vertcat(AKK,model_diet.rxns(exch_SIEC_ind(i)));
        elseif(Rxn_IDs(j)<8147)
            CLS=vertcat(CLS,model_diet.rxns(exch_SIEC_ind(i)));
        elseif(Rxn_IDs(j)<9266)
            PRE=vertcat(PRE,model_diet.rxns(exch_SIEC_ind(i)));
        elseif(Rxn_IDs(j)<10255)
            RT=vertcat(RT,model_diet.rxns(exch_SIEC_ind(i)));
        else
            SH=vertcat(SH,model_diet.rxns(exch_SIEC_ind(i)));
        end
    end
    Nbact(i)=length(find(model_diet.S(MetID,1:11945)));
end

% No. of common SIEC exchanges between the bacteria
Common=nnz(Nbact);

% 5733 rxn not included

% Finding all lumen exchanges
exc=regexp(model_diet.mets,{'\[u\]'});
exch_ind=find(~cellfun(@isempty,exc));

save('All_vars');
%% Effect of diet on bacterial secretion products

load Gut_microbiome_Western.mat

Rxns_interest=model_diet.rxns(11946:12139); % Foreign bacterial secretion products
Type=repmat({'Foreign'},length(Rxns_interest),1); % Not present in SIEC model

% Concatenating with Common (w.r.t SIEC) bacterial secretion products
Rxns_interest=vertcat(Rxns_interest,unique(vertcat(BV,DD,CP,BL,LA,AKK,CLS,PRE,RT,SH)));
Type=vertcat(Type,repmat({'Common'},length(unique(vertcat(BV,DD,CP,BL,LA,AKK,CLS,PRE,RT,SH))),1));

model1=model_diet;
Gro1(1:length(find(model1.c)))=pareto_optim(model1);
model1.lb(model1.c~=0)=Gro1(1:length(find(model1.c))); % Max growth
model1.c(:)=0;

load Gut_microbiome_High_fiber.mat
model2=model_diet;
Gro(1:length(find(model2.c)))=pareto_optim(model2);
model2.lb(model2.c~=0)=Gro(1:length(find(model2.c))); % Max growth
model2.c(:)=0;

filenm='Effect_of_Diet_Bacterial_Secretion_Prods';
[Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,Ind]...
    =FVA_analysis(model1,model2,Rxns_interest,filenm);
write_FVA_res(Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,filenm,Type(Ind))
clear Type
%% Finding Harmful bacteria secretion products (Effect of harmful bacteria on SIEC)

load Gut_microbiome_Harmful_Western.mat
Gro(1:length(find(model_diet.c)))=pareto_optim(model_diet);
model_diet.lb(model_diet.c~=0)=Gro(1:length(find(model_diet.c))); % Max growth
model_diet.c(:)=0;

Rxns_interest=model_diet.rxns(7308:7574); % Foreign bacterial secretion products
Type=repmat({'Foreign'},length(Rxns_interest),1);

% Concatenating with Common (w.r.t SIEC) bacterial secretion products
Rxns_interest=vertcat(Rxns_interest,unique(vertcat(BV,CP,CLS,RT,SH)));
Type=vertcat(Type,repmat({'Common'},length(unique(vertcat(BV,CP,CLS,RT,SH))),1));

filenm='Effect_of_harmful_bacteria_Bacterial_Secretion_Prods';
[model1_min,model1_max]=fluxVariability(model_diet,100,'max',Rxns_interest);

% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,1,'A1')
xlswrite(filenm,Rxns_interest,1,'A2')
xlswrite(filenm,model1_min,1,'B2')
xlswrite(filenm,model1_max,1,'C2')
xlswrite(filenm,Type,1,'D2')

% Saving
save(filenm);
%% Effect of beneficial bacteria on Harmful bacteria secretion products 
load Gut_microbiome_Western.mat
Gro(1:length(find(model_diet.c)))=pareto_optim(model_diet);
model_diet.lb(model_diet.c~=0)=Gro(1:length(find(model_diet.c))); % Max growth
model_diet.c(:)=0;
model1=model_diet;

load Gut_microbiome_Harmful_Western.mat
Gro(1:length(find(model_diet.c)))=pareto_optim(model_diet);
model_diet.lb(model_diet.c~=0)=Gro(1:length(find(model_diet.c))); % Max growth
model_diet.c(:)=0;
model2=model_diet;

filenm='Effect_of_beneficial_bacteria_Bacterial_Secretion_Prods';
[Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,Ind]...
    =FVA_analysis(model1,model2,Rxns_interest,filenm);
write_FVA_res(Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,filenm,Type(Ind))

%% Finding Beneficial bacteria secretion products (Effect of beneficial bacteria on SIEC)

load Gut_Microbiome_Beneficial_Western.mat
Gro(1:length(find(model_diet.c)))=pareto_optim(model_diet);
model_diet.lb(model_diet.c~=0)=Gro(1:length(find(model_diet.c))); % Max growth
model_diet.c(:)=0;

Rxns_interest=model_diet.rxns(6996:7268); % Foreign bacterial secretion products
Type=repmat({'Foreign'},length(Rxns_interest),1);

% Concatenating with Common (w.r.t SIEC) bacterial secretion products
Rxns_interest=vertcat(Rxns_interest,unique(vertcat(DD,LA,BL,AKK,PRE)));
Type=vertcat(Type,repmat({'Common'},length(unique(vertcat(DD,LA,BL,AKK,PRE))),1));

filenm='Effect_of_beneficial_bacteria_Bacterial_Secretion_Exclusive_Prods';
[model1_min,model1_max]=fluxVariability(model_diet,100,'max',Rxns_interest);

% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,1,'A1')
xlswrite(filenm,Rxns_interest,1,'A2')
xlswrite(filenm,model1_min,1,'B2')
xlswrite(filenm,model1_max,1,'C2')
xlswrite(filenm,Type,1,'D2')

% Saving
save(filenm);

%% Bacterial secretion products of each bacterium
load Gut_microbiome_Western.mat
filenm='Individual_bact_prods';

% BV
Rxns_interest=model_diet.rxns(1161:1314);
[minBV,maxBV]=fluxVariability(model_diet,100,'max',Rxns_interest);
% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,'BV','A1')
xlswrite(filenm,Rxns_interest,'BV','A2')
xlswrite(filenm,minBV,'BV','B2')
xlswrite(filenm,maxBV,'BV','C2')

% DD 2444:2581
Rxns_interest=model_diet.rxns(2310:2439);
[minDD,maxDD]=fluxVariability(model_diet,100,'max',Rxns_interest);
% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,'DD','A1')
xlswrite(filenm,Rxns_interest,'DD','A2')
xlswrite(filenm,minDD,'DD','B2')
xlswrite(filenm,maxDD,'DD','C2')

% CP 3738:3888
Rxns_interest=model_diet.rxns(3581:3718);
[minCP,maxCP]=fluxVariability(model_diet,100,'max',Rxns_interest);
% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,'CP','A1')
xlswrite(filenm,Rxns_interest,'CP','A2')
xlswrite(filenm,minCP,'CP','B2')
xlswrite(filenm,maxCP,'CP','C2')

% LA 4533:4644
Rxns_interest=model_diet.rxns(4834:4992);
[minLA,maxLA]=fluxVariability(model_diet,100,'max',Rxns_interest);
% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,'LA','A1')
xlswrite(filenm,Rxns_interest,'LA','A2')
xlswrite(filenm,minLA,'LA','B2')
xlswrite(filenm,maxLA,'LA','C2')

% BL 5581:5698
Rxns_interest=model_diet.rxns(6153:6310);
[minBL,maxBL]=fluxVariability(model_diet,100,'max',Rxns_interest);
% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,'BL','A1')
xlswrite(filenm,Rxns_interest,'BL','A2')
xlswrite(filenm,minBL,'BL','B2')
xlswrite(filenm,maxBL,'BL','C2')

% AKK 5581:5698
Rxns_interest=model_diet.rxns(7331:7429);
[minAKK,maxAKK]=fluxVariability(model_diet,100,'max',Rxns_interest);
% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,'AKK','A1')
xlswrite(filenm,Rxns_interest,'AKK','A2')
xlswrite(filenm,minAKK,'AKK','B2')
xlswrite(filenm,maxAKK,'AKK','C2')

% CLS 5581:5698
Rxns_interest=model_diet.rxns(8081:8201);
[minCLS,maxCLS]=fluxVariability(model_diet,100,'max',Rxns_interest);
% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,'CLS','A1')
xlswrite(filenm,Rxns_interest,'CLS','A2')
xlswrite(filenm,minCLS,'CLS','B2')
xlswrite(filenm,maxCLS,'CLS','C2')

% PRE 5581:5698
Rxns_interest=model_diet.rxns(9072:9190);
[minPRE,maxPRE]=fluxVariability(model_diet,100,'max',Rxns_interest);
% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,'PRE','A1')
xlswrite(filenm,Rxns_interest,'PRE','A2')
xlswrite(filenm,minPRE,'PRE','B2')
xlswrite(filenm,maxPRE,'PRE','C2')

% RT 5581:5698
Rxns_interest=model_diet.rxns(10131:10255);
[minRT,maxRT]=fluxVariability(model_diet,100,'max',Rxns_interest);
% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,'RT','A1')
xlswrite(filenm,Rxns_interest,'RT','A2')
xlswrite(filenm,minRT,'RT','B2')
xlswrite(filenm,maxRT,'RT','C2')

% SH 5581:5698
Rxns_interest=model_diet.rxns(11754:11945);
[minSH,maxSH]=fluxVariability(model_diet,100,'max',Rxns_interest);
% Writing to sheet
Legend={'Rxns','min1','max1'};
xlswrite(filenm,Legend,'SH','A1')
xlswrite(filenm,Rxns_interest,'SH','A2')
xlswrite(filenm,minSH,'SH','B2')
xlswrite(filenm,maxSH,'SH','C2')

%% Growths of bacteria as a function of beneficial bacteria %

ben_per_all=[0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9]; % Beneficial bacteria %
%ben_per_all=0.1;
 for ind=1:2 % Looping over diet
    if(ind==1)
        load Gut_Microbiome_Western.mat
    else
        
        load Gut_Microbiome_High_fiber.mat
    end
    
    for i=1:length(ben_per_all) % Looping over beneficial bacteria %
        
        ben_per=ben_per_all(i);
        model=model_diet;
        
        N_cells=length(find(model.c));
        
        harm_per=1-ben_per;
        
        % Weights for pareto-optimization
        wts=[10/11*(harm_per/5);10/11*(ben_per/5);
    10/11*(harm_per/5);10/11*(ben_per/5);
    10/11*(ben_per/5);10/11*(ben_per/5);
    10/11*(harm_per/5);10/11*(ben_per/5);
    10/11*(harm_per/5);10/11*(harm_per/5);1/11]; %for ten microbes
        % wts = 1/N_cells*ones(N_cells,1); % Equal
        
        % Find & set Max growth under these conditions
        Gro(1:N_cells,i)=pareto_optim(model,50,model.rxns(model.c~=0),wts);
        model.lb(model.c~=0)=Gro(1:N_cells,i);
        model.c(:)=0;

    end
    if(ind==1)
        Gro_West=Gro;
    else
        Gro_HF=Gro;
    end
end

figure(1)
plot(ben_per_all(1:end-2),Gro_West(4:5,1:end-2),'-',ben_per_all(1:end-2),Gro_HF(4:5,1:end-2),'-.')

legend('LA-Western','BL-Western','LA-High fiber','BL-High fiber');
xlabel('% of beneficial bacteria');
ylabel('Growth rate');
title('Effect of diet on beneficial bacteria');

print('Effect of diet on beneficial bacteria','-dpng','-r300');

figure(2)
plot(ben_per_all,Gro_West,'-',ben_per_all,Gro_HF,'-.')

legend('BV-Western','DD-Western','CP-Western','BL-Western','LA-Western',...
    'BV-High fiber','DD-High fiber','CP-High fiber','BL-High fiber','LA-High fiber');
xlabel('% of beneficial bacteria');
ylabel('Growth rate');
title('Effect of diet on bacteria');
print('Effect of diet on bacteria','-dpng','-r300');