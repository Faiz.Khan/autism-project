% Effect of oxidative stress & the leaky gut hypothesis (when toxins
% transported across organ membranes) using FVA_analysis
% Meghana V Palukuri 7/7/2017

clc
clearvars
close all
cde
tic;

Toxin_ex={'h2o2_input','h2o2_output','o2s_input','o2s_output'};

% mkdir('Results-effect of toxins');
% cd('Results-effect of toxins');
load('Toxin_ex_fluxes');
Toxin_ex_flux_gut=Toxin_ex_flux_gut_all(1,:); % Gut microbiome - Western
Toxin_ex_flux_brain=Toxin_ex_flux_brain_all(1,:);

% Setting (arbitrary) high toxin exchanges, to see effect of PBPK model
mkdir('Results-effect of toxins-without_PBPK');
cd('Results-effect of toxins-without_PBPK');
Toxin_ex_flux_gut= 100*Toxin_ex_flux_gut; 
Toxin_ex_flux_brain= 100*Toxin_ex_flux_brain;
%% SIEC rxns
load SIEC_Western.mat
Rxns_interest=model.rxns;
%% Load gut microbiome
% load Gut_Microbiome_Western1.mat
% model1=model_diet;
% %% Effect of toxins on gut microbiome
% % No toxin case (upto 50% of the SS toxin value)
% model1=changeRxnBounds(model1,Toxin_ex,0.5*Toxin_ex_flux_gut,'u'); 
% 
% Gro1(1:length(find(model1.c)))=pareto_optim(model1);
% model1.lb(model1.c~=0)=Gro1(1:length(find(model1.c))); % Max growth
% model1.c(:)=0;
% 
% % Toxin case (atleast 50% of the SS toxin value )
% model2=model_diet;
% model2=changeRxnBounds(model2,Toxin_ex,0.5*Toxin_ex_flux_gut,'l');
% 
% Gro(1:length(find(model2.c)))=pareto_optim(model2);
% model2.lb(model2.c~=0)=Gro(1:length(find(model2.c))); % Max growth
% model2.c(:)=0;
% 
% % Remove reactions absent in combined models
% Pres_rxns=find(findRxnIDs(model1,Rxns_interest)~=0);
% Rxns_interest=model.rxns(Pres_rxns);
% filenm='Effect_of_toxins_on_gut';
% [Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst]...
%    =FVA_analysis(model1,model2,Rxns_interest,filenm);
% 
% write_FVA_res(Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,filenm)

%% Load brain
load Brain_combined.mat
model1=model;

Rxns_interest=model.rxns;

%% Effect of toxins on brain
% No toxin case
model1=changeRxnBounds(model1,Toxin_ex,0.5*Toxin_ex_flux_brain,'u'); 

Gro1(1:length(find(model1.c)))=pareto_optim(model1);
model1.lb(model1.c~=0)=Gro1(1:length(find(model1.c))); % Max growth
model1.c(:)=0;

% Toxin case
model2=model;
model2=changeRxnBounds(model2,Toxin_ex,0.5*Toxin_ex_flux_brain,'l');

Gro(1:length(find(model2.c)))=pareto_optim(model2);
model2.lb(model2.c~=0)=Gro(1:length(find(model2.c))); % Max growth
model2.c(:)=0;

filenm='Effect_of_toxins_on_brain';
[Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst]...
   =FVA_analysis(model1,model2,Rxns_interest,filenm);

write_FVA_res(Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,filenm)
toc;
save('All_vars');