% Effect of diet, bacteria on SIEC metabolism using FVA_analysis
% Meghana V Palukuri 7/7/2017
clc
clearvars
close all
cde
tic;

mkdir('Result effect on SIEC rxns');
cd('Result effect on SIEC rxns');
%% SIEC rxns
load SIEC_Western.mat
Rxns_interest=model.rxns;

%% Effect of diet on SIEC
model1=model;

load SIEC_High_fiber.mat
model2=model;

filenm='Effect_of_Diet_SIEC';
[Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst]...
   =FVA_analysis(model1,model2,Rxns_interest,filenm);
write_FVA_res(Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,filenm)

%% Effect of beneficial bacteria on gut microbiome
load Gut_microbiome_Western.mat
Gro(1:length(find(model_diet.c)))=pareto_optim(model_diet);
model_diet.lb(model_diet.c~=0)=Gro(1:length(find(model_diet.c))); % Max growth
model_diet.c(:)=0;
model1=model_diet;

load Gut_microbiome_Harmful_Western.mat
Gro(1:length(find(model_diet.c)))=pareto_optim(model_diet);
model_diet.lb(model_diet.c~=0)=Gro(1:length(find(model_diet.c))); % Max growth
model_diet.c(:)=0;
model2=model_diet;

% Remove reactions absent in combined models
Pres_rxns=find(findRxnIDs(model1,Rxns_interest)~=0);
Rxns_interest=model.rxns(Pres_rxns);

filenm='Effect_of_beneficial_bacteria';
[Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst]...
   =FVA_analysis(model1,model2,Rxns_interest,filenm);
write_FVA_res(Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,filenm)

%% Effect of harmful bacteria on SIEC
model1=model;

load Gut_microbiome_Harmful_Western.mat
Gro(1:length(find(model_diet.c)))=pareto_optim(model_diet);
model_diet.lb(model_diet.c~=0)=Gro(1:length(find(model_diet.c))); % Max growth
model_diet.c(:)=0;
model2=model_diet;

% Remove reactions absent in combined models
Pres_rxns=find(findRxnIDs(model2,Rxns_interest)~=0);
Rxns_interest=model.rxns(Pres_rxns);

filenm='Effect_of_harmful_bacteria';
[Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst]...
   =FVA_analysis(model1,model2,Rxns_interest,filenm);
write_FVA_res(Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,filenm)

%% Effect of diet on gut microbiome
load Gut_microbiome_Western.mat
model1=model_diet;
Gro1(1:length(find(model1.c)))=pareto_optim(model1);
model1.lb(model1.c~=0)=Gro1(1:length(find(model1.c))); % Max growth
model1.c(:)=0;

load Gut_microbiome_High_fiber.mat
model2=model_diet;
Gro(1:length(find(model2.c)))=pareto_optim(model2);
model2.lb(model2.c~=0)=Gro(1:length(find(model2.c))); % Max growth
model2.c(:)=0;

% Remove reactions absent in combined models
Pres_rxns=find(findRxnIDs(model1,Rxns_interest)~=0);
Rxns_interest=model.rxns(Pres_rxns);

filenm='Effect_of_Diet_Gut_Microbiome';
[Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst]...
   =FVA_analysis(model1,model2,Rxns_interest,filenm);
write_FVA_res(Rxns,Mean_shift,Range_change,model1_min,model1_max,model2_min,model2_max,RxnFormulas,RxnNames,subsyst,filenm)
