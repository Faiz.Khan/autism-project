% Constructing Bacteria models in COBRA model structure format from AGORA
% reconstructions
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019
clc
clearvars
close all
cde

Bact={'Bacteroides_vulgatus_ATCC_8482',...
    'Desulfovibrio_desulfuricans_subsp_desulfuricans_DSM_642',...
    'Clostridium_perfringens_ATCC_13124',...
    'Lactobacillus_acidophilus_ATCC_4796',...
    'Bifidobacterium_longum_longum_JDM301'
    'Akkermansia_muciniphila_ATCC_BAA_835'...
    'Clostridium_difficile_NAP07'...
    'Prevotella_ruminicola_23'...
    'Ruminococcus_torques_ATCC_27756'...
    'Shigella_flexneri_2002017'}; 

diets={'Western','High_fiber'};

List=cell(length(Bact),1); % Dead end metabolites 
List_ex=cell(length(Bact),1); % Dead end metabolites in external compartment
Gro=zeros(length(Bact),1); % Biomass
Gro_diet=zeros(length(Bact),length(diets)); % Biomass under diet
msg=cell(length(Bact),1); % Leak test result

for ind=1:5
    %% Read model
    model=readCbModel(Bact{ind});
    %% Rename exchanges as per metabolites
    model=correct_exchanges(model);
    %% Add extracellular dead end metabolite exchanges
    [model,List{ind},List_ex{ind}]=add_dead_end_met_exc(model);
    %% Creating formulas field - for Reference only.Will not get changed in cobra fns.
    model.rxnFormulas=printRxnFormula(model,model.rxns,false,true,false);
    %% Check Growth
    model.c(model.c~=0)=1;
    FBAsol=optimizeCbModel(model);
    Gro(ind)=FBAsol.f;
    %% Saving model
    save(Bact{ind},'model');
    write_model(model,Bact{ind});
    %% Leak test
    [~,~,msg{ind}]=LeakTest(model);
    %% Find growth under different diets
    model_old=model;
    for i=1:length(diets)
        model=constr_model(model_old,diets{i},'(e)');
        FBAsol=optimizeCbModel(model);
        Gro_diet(ind,i)=FBAsol.f;
        save(horzcat(Bact{ind},'_',diets{i}),'model');
        %% Find growth under anaerobic condition to validate model against AGORA result
        model_anaer=constr_model(model_old,diets{i},'(e)',0);
        FBAsol_anaer=optimizeCbModel(model_anaer);
        Gro_anaer(ind,i)=FBAsol_anaer.f;
    end
end
%% Saving all variables
clear FBAsol i ind model_old
save('All_vars')