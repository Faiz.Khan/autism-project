% Construct the brain model comprising astrocyte, GABA neuron & glutamate
% neuron, by merging GABA & Glutamate neuronal models
% Meghana V Palukuri 7/7/2017
clc
clearvars
close all
cde
%% Loading individual models
load Brain_GABAergic
load Brain_glutamatergic
%% Renaming model
model_glut=rename_model(model_glut,'GluN');
model_GABA=rename_model(model_GABA,'GabaN');
%% Merging the models
model=mergeTwoModels(model_glut,model_GABA,1);
%% Removing duplicate reactions
Nrxns=length(model.rxns);
Nrxns_new=length(unique(model.rxns));
count=0;
while(Nrxns_new~=Nrxns)
    count=count+1;
    Nrxns=length(model.rxns);
    [model, removedRxnInd, keptRxnInd] = checkDuplicateRxn(model,'S');
    Nrxns_new=length(model.rxns);
end
count=count-1;
%% Modify exchanges to in/out
model=removeRxns(model,'EX_ppa(e)');
model=addReaction(model,'PPA_output',{'ppa[e]'},-1,0,0);
model=addReaction(model,'PPA_input',{'ppa[e]'},1,0,0);

model=removeRxns(model,'EX_h2o2(e)');
model=addReaction(model,'h2o2_output',{'h2o2[e]'},-1,0,0);
model=addReaction(model,'h2o2_input',{'h2o2[e]'},1,0,0);

model=removeRxns(model,'EX_o2s(e)');
model=addReaction(model,'o2s_output',{'o2s[e]'},-1,0,0);
model=addReaction(model,'o2s_input',{'o2s[e]'},1,0,0);

%% Set objective
atp_ind=strncmpi('DM_atp(c)',model.rxns,length('DM_atp(c)'));
atp=find(atp_ind);
Obj_rxns=model.rxns(atp);
model.c(atp)=1;
%% Check maintenance
[ATP_ind(1:length(atp)),ATP_sum(1:length(atp)),ATP_pareto(1:length(atp))]=get_growth(model);
%% Leak test
[~,~,msg]=LeakTest(model);
%% Saving model
save('Brain_combined','model');
write_model(model,'Brain_combined');
clear atp_ind keptRxnInd Nrxns Nrxns_new removedRxnInd
save('All_vars');