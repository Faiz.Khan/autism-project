% Construct individual neuronal type models 
% Meghana V Palukuri 7/7/2017
clc
clearvars
close all
cde
%% Brain union model

% Read model in xml format and convert to mat
model=readCbModel('NBT-CA23635B-All_Neuron_Types_Normal'); 

% Remove empty fields
model = rmfield(model,'metCharge');

% Rename metabolites having hyphens 
for i=1:length(model.mets)
model.mets{i}=strrep(model.mets{i},'-','_');
end

% Correcting exchanges
model=correct_exchanges(model);

% Add rxnFormulas
model.rxnFormulas=printRxnFormula(model,model.rxns,false,true,false);

% Leak test
[~,~,msg1]=LeakTest(model);

% Setting csense
for i=1:length(model.mets)
model.csense(i,1)='E';
end

%% Additional constraints

% Inequality ratio constraints

% 4 < DM_atp(c) + DM_atp(c)_Neuron
% model = addRatioReactionMulti(model,{'DM_atp(c)' 'DM_atp(c)_Neuron'},[1 1],'G',4);
% 
% % ASPGLUm_Neuron >= 4 * ASPGLUm
% model = addRatioReaction(model,{'ASPGLUm_Neuron' 'ASPGLUm'},[1 4],'L');
% 
% % 33.3 *ALAtN1_Neuron < (ALATA_Lm_Neuron + ALATA_L_Neuron)
% model = addRatioReactionMulti(model,{'ALAtN1_Neuron' 'ALATA_Lm_Neuron' 'ALATA_L_Neuron'},[-33.3 1 1],'G');
%  
% % .03 * CYOOm2_Neuron >= LSOProd_Neuron >= .01 * CYOOm2_Neuron
% model = addRatioReaction(model,{'CYOOm2_Neuron' 'LSOProd_Neuron'},[0.03 1],'L');
% model = addRatioReaction(model,{'LSOProd_Neuron' 'CYOOm2_Neuron'},[1 0.01],'L');
% 
% % .03 * CYOOm2 >= LSOProd >= .01 * CYOOm2 
% model = addRatioReaction(model,{'CYOOm2' 'LSOProd'},[0.03 1],'L');
% model = addRatioReaction(model,{'LSOProd' 'CYOOm2'},[1 0.01],'L');
% 
% % 10 * PDHm >= PDHm_Neuron >= 2*PDHm
% model = addRatioReaction(model,{'PDHm' 'PDHm_Neuron'},[10 1],'L');
% model = addRatioReaction(model,{'PDHm_Neuron' 'PDHm'},[1 2],'L');
% 
% % Equality ratio constraint
% model = addRatioReaction(model,{'GLCt1r' 'GLCt1r_Neuron'},[155 27]);
% 
% % Checking ATP production
% FBAsol=optimizeCbModel(model);
% ATP_union=FBAsol.f;
% 
% save('Brain_union.mat','model');

%% GABAergic model

% Remove reactions not present in this model
rxnRemoveList_GABA={'ACHVESSEC_Neuron'
'CHAT_Neuron'
'CHOLt4_Neuron'
'ACHEe_Int'
'ACITL_Neuron'
'GLUVESSEC_Neuron'};
model_GABA= removeRxns(model,rxnRemoveList_GABA);

% Removing associated genes
c=0;
for i=1:length(model_GABA.genes)
    effected_genes=find(model_GABA.rxnGeneMat(:,i));
    if(isempty(effected_genes))
        c=c+1;
      model_GABA=deleteModelGenes(model_GABA,model_GABA.genes(i));
    end
end

% Adding bounds as per model specs
model_GABA=changeRxnBounds(model_GABA,'ABUTt2r_Int',0.2,'l');

% Adding demand reaction for GABA for future analysis
model_GABA=addDemandReaction(model_GABA,'4abut[cN]');

% Leak test
[~,~,msg3]=LeakTest_SS(model_GABA);

% Checking ATP production
FBAsol=optimizeCbModel(model_GABA);
ATP_GABA=FBAsol.f;

save('Brain_GABAergic','model_GABA');
%% Glutamatergic model

% Remove reactions not present in this model
rxnRemoveList_glut={'ACHVESSEC_Neuron'
'CHAT_Neuron'
'CHOLt4_Neuron'
'GABAVESSEC_Neuron'
'GLUDC_Neuron'
'ACHEe_Int'
'ACITL_Neuron'};
model_glut= removeRxns(model,rxnRemoveList_glut);

% Removing associated genes
c=0;
for i=1:length(model_glut.genes)
    effected_genes=find(model_glut.rxnGeneMat(:,i));
    if(isempty(effected_genes))
        c=c+1;
      model_glut=deleteModelGenes(model_glut,model_glut.genes(i));
    end
end

% Adding bounds as per model specs
model_glut=changeRxnBounds(model_glut,'GLUVESSEC_Neuron',0.25,'l');

% Adding demand reaction for glutamate for future analysis
model_glut=addDemandReaction(model_glut,'glu_L[cN]');

% Leak test
[~,~,msg4]=LeakTest_SS(model_glut);

% Checking ATP production
FBAsol=optimizeCbModel(model_glut);
ATP_glut=FBAsol.f;

save('Brain_glutamatergic','model_glut');

%% Saving
clear i FBAsol
save All_vars.mat