function model = setDiet(model,diet,ext_comp)
% Set model intake according to dietary constraints
%
% model = setDiet(model,diet,ext_comp)
% Requires the spreadsheet Western.xls or High_fiber.xls containing dietary
% constraints to be on the MATLAB path
%
% INPUT
% model             COBRA model structure
% diet              Diet name - 'Western' or 'High_fiber'
%
%OPTIONAL INPUTS
% ext_comp          External component (default - (u))
%
% OUTPUT
% model             model after setting diet
%
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019
if(nargin<3)
    ext_comp='[u]';
end

%SIEC_DW=133.9704;
%Brain_WW=0.7*1407;
%Organ_DW=SIEC_DW;

switch diet
    case {'Western','High_fiber'}
        exc=strncmpi('EX_',model.rxns,2);     % Finding all exchanges   
        model.lb(exc)=0; % Making all exchanges 0
        
        diet=strcat(diet,'.xls'); % Reading diet constraints from spreadsheet
        [flux_val,exc]=xlsread(diet);
        %flux=flux_val/(24*Organ_DW);
        flux=flux_val;
        
        for i = 1 : length(exc)
            exc{i}= strrep(exc{i},'[u]',ext_comp);
            
            % Setting exchange bounds as per diet
            model=changeRxnBounds(model,exc{i},flux(i),'l'); 
        end
        
    otherwise
        error('Diet not recognized.')
        
end

end
