% Construct gut micrbiome models 
% Meghana V Palukuri and Faiz K. Mohammed 13/06/2019
clc
clearvars
close all
cde
%% Loading individual models
load SIEC
model_gut=model;

Bact={'Bacteroides_vulgatus_ATCC_8482',...
    'Desulfovibrio_desulfuricans_subsp_desulfuricans_DSM_642',...
    'Clostridium_perfringens_ATCC_13124',...
    'Lactobacillus_acidophilus_ATCC_4796',...
    'Bifidobacterium_longum_longum_JDM301'
    'Akkermansia_muciniphila_ATCC_BAA_835'...
    'Clostridium_difficile_NAP07'...
    'Prevotella_ruminicola_23'...
    'Ruminococcus_torques_ATCC_27756'...
    'Shigella_flexneri_2002017'};

load(Bact{1});
model_BV=model;
load(Bact{2});
model_DD=model;
load(Bact{3});
model_CP=model;
load(Bact{4});
model_LA=model;
load(Bact{5});
model_BL=model;
load(Bact{6});
model_AKK=model;
load(Bact{7});
model_CLS=model;
load(Bact{8});
model_PRE=model;
load(Bact{9});
model_RT=model;
load(Bact{10});
model_SH=model;
clear model

Model_name={'Gut_Microbiome','Gut_Microbiome_Harmful','Gut_Microbiome_Beneficial'};
diets={'Western','High_fiber'};

% Biomass of each cell under different diets/ found by different methods
Gro_diet=zeros(length(Bact)+1,length(diets),length(Model_name));
Gro_ind=zeros(length(Bact)+1,length(Model_name));
Gro_sum=zeros(length(Bact)+1,length(Model_name));
Gro_pareto=zeros(length(Bact)+1,length(Model_name));

% Msg giving result of Leak test
msg=cell(length(Model_name),1);

for ind=1:length(Model_name)
    if(ind==1)
        % Complete microbiome
        models={model_BV; model_DD; model_CP; model_LA; model_BL;model_AKK;model_CLS;model_PRE;model_RT;model_SH};
        nameTagsModels={'BV_';'DD_';'CP_';'LA_';'BL_';'AKK_';'CLS_';'PRE_';'RT_';'SH_'};
    elseif(ind==2)
        % Harmful microbiome
        models={model_BV; model_CP;model_CLS;model_RT;model_SH};
        nameTagsModels={'BV_';'CP_';'CLS_';'RT_';'SH_'};
    else
        % Beneficial microbiome
        models={ model_DD;model_LA; model_BL;model_AKK;model_PRE};
        nameTagsModels={'DD_';'LA_';'BL_';'AKK_';'PRE_'};
    end
    %% Bacterial system model
    [modelJoint] = createMultipleSpeciesModel(models,nameTagsModels);
    %% Correcting exchanges
%     modelJoint=correct_exchanges(modelJoint);
    %% Merging with gut
    [model] = mergeTwoModels(modelJoint,model_gut,1);
    %% Removing duplicate reactions
    [model, removedRxnInd, keptRxnInd] = checkDuplicateRxn(model,'S');
    %% Check growth
    Tags=nameTagsModels;
    nm1=strcat(Tags,'biomass');
    nm=vertcat(nm1,'biomass');
    
    biom=zeros(length(nm),1);
    for i=1:length(nm)
        biom_ind=strncmpi(nm{.i},model.rxns,length(nm{i}));
        biom(i)=find(biom_ind);
    end
    Growth_rxns=model.rxns(biom);
    [si,ze]=size(model.S); %addition
    model.b=zeros([si,1]) %addition
    model.c(biom)=1;    
    [Gro_ind(1:length(biom),ind),Gro_sum(1:length(biom),ind),Gro_pareto(1:length(biom),ind)]=get_growth(model);
    %% Creating formulas field - for Reference only.Will not get changed in cobra fns.
    model.rxnFormulas=printRxnFormula(model,model.rxns,false,true,false);    
    %% Growth under different diets
    for i=1:length(diets)
        model_diet=constr_model(model,diets{i});
        Gro_diet(1:length(biom),i,ind)=pareto_optim(model_diet);
        save(horzcat(Model_name{ind},'_',diets{i}),'model_diet');
    end
    %% Leak test
    [~,~,msg{ind}]=LeakTest(model);
    %% Saving
    save(Model_name{ind},'model');
    write_model(model,Model_name{ind});
end
clear biom_ind i models nm Tags nm1 nameTagsModels
save('All_var');
