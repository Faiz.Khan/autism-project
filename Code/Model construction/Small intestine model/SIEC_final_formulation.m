% Update SIEC model
% Meghana V Palukuri 7/7/2017
clc
clearvars
close all
cde
%% Load original model
load ImprovedsIECModel.mat
%% Renaming exchanges as per metabolites
model=correct_exchanges(model); 
%% Adding propionate rxns
model=addMetabolite(model,'ppa[e]','Propionate (n-C3:0)','C3H5O2');
model=addMetabolite(model,'ppa[u]','Propionate (n-C3:0)','C3H5O2');
model=addMetabolite(model,'ppa[c]','Propionate (n-C3:0)','C3H5O2');
model=addMetabolite(model,'ppa[m]','Propionate (n-C3:0)','C3H5O2');

amp=strncmpi('amp',model.mets,3);
amp=find(amp);
model=addMetabolite(model,'amp[m]',model.metNames(amp(1)),model.metFormulas(amp(1)));
model = addReaction(model,{'ADK1m','adenylate kinase, mitochondrial'},'amp[m] + atp[m]  <=> 2 adp[m]',[],1,-1000,1000,0,'Propanoate metabolism');

model = addReaction(model,{'ACCOALm','acetate-CoA ligase (AMP-forming)'},'atp[m] + coa[m] + ppa[m] -> amp[m] + ppi[m] + ppcoa[m] ',[],0,0,1000,0,'Propanoate metabolism');
model = addReaction(model,{'PPAt','Propionate transport, diffusion'},'ppa[u] -> ppa[c]',[],0,0,1000,0,'Transport, extracellular');
model = addReaction(model,{'PPAtm','Propionate transport, diffusion'},'ppa[c] -> ppa[m]',[],0,0,1000,0,'Transport, mitochondrial');
model = addReaction(model,{'EX_ppa(e)','Propionate exchange'},'ppa[e] <=> ',[],1,-1000,1000,0,'Exchange/demand reaction');
model = addReaction(model,{'PPAet','Propionate transport, diffusion'},'ppa[c] -> ppa[e]',[],0,0,1000,0,'Transport, extracellular');
model = addReaction(model,{'EX_ppa(u)','Propionate exchange'},'ppa[u] <=> ',[],1,-1000,1000,0,'Exchange/demand reaction');
%% Adding ROS exchange reactions
h2o2=strncmpi('h2o2',model.mets,length('h2o2'));
h2o2=find(h2o2);
sox=strncmpi('o2s',model.mets,length('o2s'));
sox=find(sox);

model=addMetabolite(model,'h2o2[e]',model.metNames(h2o2(1)),model.metFormulas(h2o2(1)));
model=addMetabolite(model,'o2s[c]',model.metNames(sox),model.metFormulas(sox));
model=addMetabolite(model,'o2s[e]',model.metNames(sox),model.metFormulas(sox));

model = addReaction(model,{'EX_o2s(e)','Superoxide exchange'},'o2s[e] <=> ',[],1,-1000,1000,0,'Exchange/demand reaction');
model = addReaction(model,{'O2St','Superoxide transport, diffusion'},'o2s[c] <=> o2s[e]',[],1,-1000,1000,0,'Transport, extracellular');
model = addReaction(model,{'O2Stm','Superoxide transport, diffusion'},'o2s[c] <=> o2s[m]',[],1,-1000,1000,0,'Transport, mitochondrial');
model = addReaction(model,{'H2O2te','Hydrogen peroxide transport, diffusion'},'h2o2[c] -> h2o2[e]',[],0,0,1000,0,'Transport, extracellular');
model = addReaction(model,{'EX_h2o2(e)','Hydrogen peroxide exchange'},'h2o2[e] <=> ',[],1,-1000,1000,0,'Exchange/demand reaction');
%% Modify exchanges as input/output
% Change rxns
model=removeRxns(model,'EX_ppa(e)');
model=addReaction(model,'PPA_output',{'ppa[e]'},-1,0,0);
model=addReaction(model,'PPA_input',{'ppa[e]'},1,0,0);

model=removeRxns(model,'EX_h2o2(e)');
model=addReaction(model,'h2o2_output',{'h2o2[e]'},-1,0,0);
model=addReaction(model,'h2o2_input',{'h2o2[e]'},1,0,0);

model=removeRxns(model,'EX_o2s(e)');
model=addReaction(model,'o2s_output',{'o2s[e]'},-1,0,0);
model=addReaction(model,'o2s_input',{'o2s[e]'},1,0,0);

%% Creating formulas field - for Reference only.Will not get changed in cobra fns.
model.rxnFormulas=printRxnFormula(model,model.rxns,false,true,false);
%% Check Growth
model_obj=strncmpi('biomass',model.rxns,length('biomass'));
model.c(model_obj)=1;
FBAsol=optimizeCbModel(model);
Gro=FBAsol.f;
%% Saving model & writing to sheet
save('SIEC.mat','model');
write_model(model,'SIEC');
%% Check for flux through added reactions
rxnNameList=model.rxns(1283:end);
[minFlux,maxFlux] = fluxVariability(model,100,'max',rxnNameList);
if(find(minFlux==0 & maxFlux==0))
    Blocked_rxns=rxnNameList(minFlux==0 & maxFlux==0);
    msg2=vertcat('Blocked rxn:',Blocked_rxns);
else
    msg2='New rxns can carry flux';
end
disp(msg2);
%% Leak test
[~,~,msg]=LeakTest(model);
%% Setting diet & Checking growth
model_old=model;
diets={'Western','High_fiber'};
Gro_diet=zeros(length(diets),1);
for i=1:length(diets)
    model=constr_model(model_old,diets{i});
    FBAsol=optimizeCbModel(model);
    Gro_diet(i)=FBAsol.f;
    save(horzcat('SIEC_',diets{i}),'model');
end
%% Saving all variables
clear FBAsol i model_old 
save('All_vars')