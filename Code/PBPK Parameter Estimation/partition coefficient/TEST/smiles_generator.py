import requests
import json

# compounds from yun-edington and rodgers (2005) and rodgers (2006) and jansson for rats
compounds = ["Propionic acid", "Hydrogen sulfide", "superoxide"]

f = open("smiles_file.smi", "w")
print(len(compounds))

for c in compounds:
	r = requests.get("https://pubchem.ncbi.nlm.nih.gov/rest/pug" + "/compound/name/" + c + "/property/CanonicalSMILES" + "/JSON")
	s = r.json()["PropertyTable"]["Properties"][0]["CanonicalSMILES"] + " " + c.replace(" ", "_") + "\n"
	print(s)
	f.write(s)

f.close()
	

