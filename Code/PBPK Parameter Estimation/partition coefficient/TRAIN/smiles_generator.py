import requests
import json

# compounds from yun-edington and rodgers (2005) and rodgers (2006) and jansson for rats
compounds = ["R-Acebutolol", "S-Acebutolol", "Dextrobetaxolol", "Levobetaxolol", "(r)-bisoprolol", "(s)-bisoprolol", "caffeine", "(R)-Carvedilol", "chlorpromazine", "cocaine", "cotinine", "haloperidol", "inaperisone", "lidocaine", "(R)-Metoprolol", "(S)-Metoprolol", "morphine", "nicotine", "pentazocine", "pethidine", "(R)-Pindolol", "(S)-Pindolol", "procainamide", "propranolol", "pyridostigmine", "theophyllin", "verapamil", "quinidine", "(S)-timolol", "enoxacin", "ofloxacin", "tetracycline", "pefloxacin", "domperidone", "prucalopride", "sabeluzole", "lubeluzole", "laniquidar", "nebivolol", "sufentanil", "galantamine", "loperamide", "cisapride", "ketanserin", "risperidone", "levocabastine", "norfloxacin", "grepafloxacin", "sparfloxacin", "penicillin", "salicylic acid", "valproic acid", "glycyrrhizin", "tenoxicam", "fleroxacin", "hexobarbital", "phenytoin", "nalidixic acid", "ftorafur", "2,3-dideoxyinosine", "ethoxybenzamide", "digoxin", "prednisolone", "clobazam", "cyclosporin", "propofol", "triazolam", "alprazolam", "chlordiazepoxide", "midazolam", "ridogrel", "biperiden", "(S)-Carvedilol", "fentanyl", "Lorcainide", "imipramine", "phencyclidine", "lomefloxacin", "pipemidic acid", "disopyramide", "thiopental", "tolbutamide", "cefazolin", "ceftazidime", "bromperidol", "pentobarbital", "flunitrazepam", "mazapertine", "alfentanil", "diazepam", "R-Etodolac", "S-Etodolac", "phenobarbital", "hexobarbitone", "methicillin", "dicloxacillin", "trans-Retinoic Acid", "clomipramine", "clotiazepam", "(S)-Flecainide", "(R)-Flecainide", "nitrazepam", "promethazine", "trihexyphenidyl", "thiopentone", "2,2-DIMETHYLBUTANE", "PENTANE", "2-METHYLPENTANE", "3-METHYLPENTANE", "HEXANE", "3-METHYLHEXANE", "HEPTANE", "CYCLOPROPANE", "METHYLCYCLOPENTANE", "CYCLOHEXANE", "BENZENE", "TOLUENE", "STYRENE", "DICHLOROMETHANE", "TRICHLOROMETHANE", "1,1,1-TRICHLOROETHANE", "2-Chloro-1,1,1-trifluoroethane", "TEFLURANE", "HALOTHANE", "2-Chloro-1,1-difluoroethene", "1,1,2-trichloroethene", "methanol", "ethanol", "1-propanol", "2-propanol", "1-butanol", "acetone", "butanone", "diethyl ether", "divinyl ether", "fluroxene", "enflurane", "isoflurane", "methoxyflurane", "sevoflurane"]    

f = open("smiles_file.smi", "w")
print(len(compounds))

for c in compounds:
	r = requests.get("https://pubchem.ncbi.nlm.nih.gov/rest/pug" + "/compound/name/" + c + "/property/CanonicalSMILES" + "/JSON")
	s = r.json()["PropertyTable"]["Properties"][0]["CanonicalSMILES"] + " " + c.replace(" ", "_") + "\n"
	print(s)
	f.write(s)

f.close()
	

