import numpy, math
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn.linear_model import LinearRegression, Lasso, LassoCV
from sklearn import cross_validation

f1 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/TRAIN/descriptors_complete_brain", "r")
f2 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/TRAIN/partition_coefficient_brain", "r")
f3 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/TEST/descriptors_complete_brain", "r")

X = numpy.zeros((140,1449))
Y = numpy.zeros((140, 1))
X_test = numpy.zeros((3, 1449))
f1.readline()
f3.readline()

missing_op = set()

row = 0
for line in f1:
	pos = line.rfind("\"")
	l = line[pos+2:-1].split(",")
	X[row,:] = [0.0 if i=="" else float(i) for i in l]
	row = row+row

row = 0
for line in f3:
	pos = line.rfind("\"")
	l = line[pos+2:-1].split(",")
	X_test[row,:] = [0.0 if i=="" or i=="Infinity" or i=="-Infinity" else float(i) for i in l]
	row = row+1

row = 0
for line in f2:
	# print line
	if line=="-1\n":
		Y[row,0] = numpy.nan
		missing_op.add(row)
	else:
		Y[row,0] = float(line)
	row = row+1


X = numpy.delete(X, list(missing_op), 0)
Y = numpy.delete(Y, list(missing_op), 0)
print X.shape, Y.shape

folds = 5
fold_size = (int)(len(X[:,0])/folds)

alphas = numpy.arange(0.0001,0.01,0.0001)

# mse_alpha = []
# for alpha in alphas:
# 	model_lasso = Lasso(alpha=alpha,fit_intercept=True)
# 	err_sum=0
# 	for i in range(folds):
# 		X_CV_test = X[i*fold_size:(i+1)*fold_size,:]
# 		Y_CV_test = Y[i*fold_size:(i+1)*fold_size,:]
# 		X_CV_train = numpy.concatenate((X[:i*fold_size,:], X[(i+1)*fold_size:,:]), axis=0)
# 		Y_CV_train = numpy.concatenate((Y[:i*fold_size,:], Y[(i+1)*fold_size:,:]), axis=0)
# 		model_lasso.fit(X_CV_train, Y_CV_train)
# 		err_sum = err_sum + numpy.sum((model_lasso.predict(X_CV_train) - Y_CV_train)**2)
# 	mse_alpha.append(err_sum/len(X[:,0]))
# print mse_alpha
# plt.figure(1)
# plt.plot(alphas, mse_alpha, 'ro')

mse_per_fold = numpy.zeros((folds,len(alphas)))
for i in range(folds):
	X_CV_test = X[i*fold_size:(i+1)*fold_size,:]
	Y_CV_test = Y[i*fold_size:(i+1)*fold_size,:]
	X_CV_train = numpy.concatenate((X[:i*fold_size,:], X[(i+1)*fold_size:,:]), axis=0)
	Y_CV_train = numpy.concatenate((Y[:i*fold_size,:], Y[(i+1)*fold_size:,:]), axis=0)
	for idx,alpha in enumerate(alphas):
		model_lasso = Lasso(alpha=alpha)
		model_lasso.fit(X_CV_train, Y_CV_train)
		mse_per_fold[i,idx] = numpy.mean((model_lasso.predict(X_CV_train) - Y_CV_train)**2)

plt.figure(3)
for f in range(folds):
	plt.plot(alphas, mse_per_fold[f,:], ':')
average_mse = []
for idx,a in enumerate(alphas):
	average_mse.append(numpy.mean(mse_per_fold[:,idx]))
plt.plot(alphas, average_mse, 'k')

model_lasso = Lasso(alpha=0.0001)
model_lasso.fit(X, Y)
for idx,c in enumerate(model_lasso.coef_):
	if c!=0:
		print idx, c
print(model_lasso.predict(X_test))

# regr = LinearRegression()
# pca = PCA()
# directions = numpy.arange(1,20)
# X_reduced = pca.fit_transform(scale(X))
# mse_PCR = numpy.zeros((folds, len(directions)))
# for i in range(folds):
# 	X_CV_test = X_reduced[i*fold_size:(i+1)*fold_size,:]
# 	Y_CV_test = Y[i*fold_size:(i+1)*fold_size,:]
# 	X_CV_train = numpy.concatenate((X_reduced[:i*fold_size,:], X_reduced[(i+1)*fold_size:,:]), axis=0)
# 	Y_CV_train = numpy.concatenate((Y[:i*fold_size,:], Y[(i+1)*fold_size:,:]), axis=0)
# 	for d in directions:
# 		regr.fit(X_CV_train[:,:d], Y_CV_train.ravel())
# 		mse_PCR[i,d-1] = numpy.mean((regr.predict(X_CV_test[:,:d]) - Y_CV_test)**2)

# print mse_PCR.shape
# plt.figure(4)
# for f in range(folds):
# 	plt.plot(mse_PCR[f,:], ':')
# plt.show()
# average_mse_PCR = []
# for d in directions:
# 	average_mse_PCR.append(numpy.mean(mse_PCR[:,d]))
# plt.plot(average_mse_PCR, 'k')

	


plt.show()

# pca = PCA()
# X_reduced = pca.fit_transform(scale(X))
# N = len(X_reduced)
# k_5 = cross_validation.KFold(N, n_folds=5, shuffle=True)
# regr = LinearRegression()
# mse = []
# score = -1*cross_validation.cross_val_score(regr, numpy.ones((N,1)), Y.ravel(), cv=k_5, scoring='mean_squared_error').mean()    
# mse.append(score)
# for i in numpy.arange(1,20):
# 	score = -1*cross_validation.cross_val_score(regr, X_reduced[:,:i], Y.ravel(), cv=k_5, scoring='mean_squared_error').mean()
# 	mse.append(score)

# print mse
# plt.plot(mse, '-v')
# plt.show()

# val = min(mse)
# idx = mse.index(val)

# print val, idx

# regr.fit(X_reduced[:,:idx], Y.ravel())
# print len(regr.coef_)
# X_test = pca.fit_transform(scale(X_test))[:,:idx]
# print X_test.shape
# print(regr.predict(X_test))

# plt.plot(mse, '-v')
# plt.xlabel("Number of components")
# plt.ylabel("MSE")
# plt.show()

# 4.28155978  2.08089957