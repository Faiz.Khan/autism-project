# f = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/descriptors_file_incomplete", "r")
f = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/TEST/descriptors_file_incomplete", "r")
# f1 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/descriptors_complete_kidney","w")
f1 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/TEST/descriptors_complete_brain","w")
f2 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/TEST/descriptors_complete_heart","w")
f3 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/TEST/descriptors_complete_adipose","w")
f4 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/TEST/descriptors_complete_liver","w")
f5 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/TEST/descriptors_complete_gut","w")
f6 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/qsar_code/TEST/descriptors_complete_kidney","w")

s = f.readline()
f1.write(s[:-1] + ",fw_brain,fnl_brain,fnp_brain,alb_ratio_brain,lip_protein_ratio_brain\n")
f2.write(s[:-1] + ",fw_heart,fnl_heart,fnp_heart,alb_ratio_heart,lip_protein_ratio_heart\n")
f3.write(s[:-1] + ",fw_adipose,fnl_adipose,fnp_adipose,alb_ratio_adipose,lip_protein_ratio_adipose\n")
f4.write(s[:-1] + ",fw_liver,fnl_liver,fnp_liver,alb_ratio_liver,lip_protein_ratio_liver\n")
f5.write(s[:-1] + ",fw_gut,fnl_gut,fnp_gut,alb_ratio_gut,lip_protein_ratio_gut\n")
f6.write(s[:-1] + ",fw_kidney,fnl_kidney,fnp_kidney,alb_ratio_kidney,lip_protein_ratio_kidney\n")

for line in f:
	print 's'
	f1.write(line[:-1] + ",0.753,0.0391,0.0015,0.048,0.041\n")
	f2.write(line[:-1] + ",0.568,0.0135,0.0106,0.157,0.160\n")
	f3.write(line[:-1] + ",0.144,0.0016,0.853,0.049,0.068\n")
	f4.write(line[:-1] + ",0.642,0.0135,0.0238,0.086,0.161\n")
	f5.write(line[:-1] + ",0.738,0.0375,0.0124,0.158,0.141\n")
	f6.write(line[:-1] + ",0.672,0.0121,0.0240,0.130,0.137\n")
	
f.close() 
f1.close()
f2.close()
f3.close()
f4.close()
f5.close()
f6.close()


# Brain : ",0.753,0.0391,0.0015,0.048,0.041\n"
# Heart : ",0.568,0.0135,0.0106,0.157,0.160\n"
# Adipose : ",0.144,0.0016,0.853,0.049,0.068\n"
# Liver : ",0.642,0.0135,0.0238,0.086,0.161\n"
# Gut : ",0.738,0.0375,0.0124,0.158,0.141\n"
# Kidney : ",0.672,0.0121,0.0240,0.130,0.137\n"
