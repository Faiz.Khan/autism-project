import numpy, math, csv
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn.linear_model import LinearRegression, Lasso, LassoCV
from sklearn import cross_validation
from sklearn import tree

f1 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/renal clearance/descriptors_file", "r")
f3 = open("/home/shruti/Documents/Academics/PROJECT/PBBK/QSAR/renal clearance/TEST/descriptors_file", "r")

X = numpy.zeros((390, 1444))
Y = numpy.zeros((390,1))
X_test = numpy.zeros((3, 1444))

f1.readline()
f3.readline()

row = 0
for line in f1:
	pos = line.rfind("\"")
	l = line[pos+2:-1].split(",")
	X[row,:] = [0.0 if i=="" or i=="Infinity" or i=="-Infinity" else float(i) for i in l]
	row = row+1

row = 0
for line in f3:
	pos = line.rfind("\"")
	l = line[pos+2:-1].split(",")
	X_test[row,:] = [0.0 if i=="" or i=="Infinity" or i=="-Infinity" else float(i) for i in l]
	row = row+1

row = 0
with open('clearance_values.csv', 'rb') as f2:
	reader = csv.reader(f2)
	for r in reader:
		Y[row,0] = r[0]
		row = row+1


# folds = 5
# fold_size = (int)(len(X[:,0])/folds)

# alphas = numpy.arange(0.1,1,0.1)

# mse_per_fold = numpy.zeros((folds,len(alphas)))
# for i in range(folds):
# 	X_CV_test = X[i*fold_size:(i+1)*fold_size,:]
# 	Y_CV_test = Y[i*fold_size:(i+1)*fold_size,:]
# 	X_CV_train = numpy.concatenate((X[:i*fold_size,:], X[(i+1)*fold_size:,:]), axis=0)
# 	Y_CV_train = numpy.concatenate((Y[:i*fold_size,:], Y[(i+1)*fold_size:,:]), axis=0)
# 	for idx,alpha in enumerate(alphas):
# 		model_lasso = Lasso(alpha=alpha, max_iter=10000, tol=0.01)
# 		model_lasso.fit(X_CV_train, Y_CV_train)
# 		mse_per_fold[i,idx] = numpy.mean((model_lasso.predict(X_CV_train) - Y_CV_train)**2)

# plt.figure(3)
# for f in range(folds):
# 	plt.plot(alphas, mse_per_fold[f,:], ':')
# average_mse = []
# for idx,a in enumerate(alphas):
# 	average_mse.append(numpy.mean(mse_per_fold[:,idx]))
# plt.plot(alphas, average_mse, 'k')

# model_lasso = Lasso(alpha=0.0001)
# model_lasso.fit(X, Y)
# for idx,c in enumerate(model_lasso.coef_):
# 	if c!=0:
# 		print idx, c
# print(model_lasso.predict(X_test))


# regr = LinearRegression()
# pca = PCA()
# directions = numpy.arange(1,21)
# X_reduced = pca.fit_transform(scale(X))
# mse_PCR = numpy.zeros((folds, len(directions)))
# for i in range(folds):
# 	X_CV_test = X_reduced[i*fold_size:(i+1)*fold_size,:]
# 	Y_CV_test = Y[i*fold_size:(i+1)*fold_size,:]
# 	X_CV_train = numpy.concatenate((X_reduced[:i*fold_size,:], X_reduced[(i+1)*fold_size:,:]), axis=0)
# 	Y_CV_train = numpy.concatenate((Y[:i*fold_size,:], Y[(i+1)*fold_size:,:]), axis=0)
# 	for d in directions:
# 		regr.fit(X_CV_train[:,:d], Y_CV_train.ravel())
# 		mse_PCR[i,d-1] = numpy.mean((regr.predict(X_CV_test[:,:d]) - Y_CV_test)**2)

# print mse_PCR.shape
# plt.figure(4)
# for f in range(folds):
# 	plt.plot(mse_PCR[f,:], ':')
# plt.figure(5)
# average_mse_PCR = []
# for d in directions:
# 	average_mse_PCR.append(numpy.mean(mse_PCR[:,d-1]))
# plt.plot(average_mse_PCR, 'k')

# plt.show()


# regr = LinearRegression()
# regr.fit(X, Y)
# print('Coefficients: \n', regr.coef_)
# print(regr.predict(X_test))

clf = tree.DecisionTreeRegressor()
clf = clf.fit(X, Y)
print(clf.predict(X_test))