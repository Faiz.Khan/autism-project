import requests
import json
import csv


compounds = []
with open('compounds.csv', 'rb') as f:
	reader = csv.reader(f)
	for row in reader:
		compounds.append(row[0])
f.close()
print(len(compounds))
# compounds = ["Propionic acid", "Hydrogen peroxide", "Superoxide"]

f = open("smiles_file.smi", "w")
# f = open("TEST\smiles_file_test.smi", "w")
for c in compounds:
	r = requests.get("https://pubchem.ncbi.nlm.nih.gov/rest/pug" + "/compound/name/" + c + "/property/CanonicalSMILES" + "/JSON")
	if(r.json().get("Fault")):
		print c + " missing\n"
	else:
		s = r.json()["PropertyTable"]["Properties"][0]["CanonicalSMILES"] + " " + c.replace(" ", "_") + "\n"
		print(s)
		f.write(s)
f.close()
	

