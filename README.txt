PROJECT: Integrated COBRA-PBPK Model to study interactions between the Gut Microbiome and the Brain in Autism

MOTIVATION: Autism is a complex neurological disorder affecting 1 in 59 children in the US. Recent evidence of increased observation of gastro-intestinal disturbances in autistic children, followed by experimental characterization of abnormal levels of gut microbiota has motivated computational studies of the gut-brain link with respect to autism.  

CODE SUMMARY: Construction, integration and analyis of a hybrid model comprising constraint based models of the small intestinal epithelial cell, 5 different gut bacteria cells and a representative neuronal cell; and a PBPK transport model. 

-------------------------------------------------------------------------------------------------------------------------------
Code Requirements: 

1. MATLAB R2017b was used for development, however MATLAB R2014b and above should work too, 
2. COBRA toolbox - https://github.com/opencobra/cobratoolbox
3. Gurobi optimizer
4. SBML toolbox (recommended)

-------------------------------------------------------------------------------------------------------------------------------
USAGE:

1. Install all the above components, and ensure they are working in tandem. For instance, ensure that GUROBI, COBRA toolbox are added to the MATLAB path (call addpath) and run basic COBRA toolbox tests like FBA. Call changeCobraSolver if gurobi isn't being used. 

2. IMPORTANT: 

(i) Add the entire folder 'CODE' to MATLAB path, including all files in all subfolders. This folder should contain the folders Model Analysis, Integrated_model_oxidative_stress, COBRA Function files, COBRA edited functions*, PBPK Parameter Estimation**, Model construction. 

(ii) EITHER run all scripts in the folder Model Construction (except in subfolder Diets) and add them to MATLAB Path OR Add all the models in the Folder Supplementary information/Models of the paper draft to the MATLAB Path. Ensure that the names of the models are Gut_Microbiome_Beneficial.mat , Gut_Microbiome_Beneficial_High_fiber.mat, Gut_Microbiome_Beneficial_Western.mat, Gut_Microbiome_Harmful.mat, Gut_Microbiome_Harmful_High_fiber.mat, Gut_Microbiome_Harmful_Western.mat, Gut_Microbiome_High_fiber.mat, Gut_Microbiome_Western.mat, Gut_Microbiome.mat , Brain_combined.mat

3. Run all scripts in the MATLAB GUI. 

4. Results of many of the scripts will be stored in a folder 'Results' created in the directory where MATLAB is running the code (generally, the current directory in the MATLAB editor). Strongly recommend changing directory before running the script to a folder of your choice.

* ENSURE that files from the folder COBRA edited functions are being called when you run scripts in this project, instead of the original COBRA toolbox scripts. 

**This folder and its contents don't need to be added. 
---------------------------------------------------------------------------------------------------------------------------------
REGENERATING RESULTS:

Replicating integrated model results - concentration-time graphs, and toxin flux, concentration values spreadsheet: 

1. Run the script Integrated_model_oxidative_stress/Integrated.m after specifying all models of interest - this can be set in the section of the code %%Load gut model

2. Generating results for different conditions can be done by modifying the required parameters in Integrated.m
Important handles include the following in sections/comments: %% Initial condns, % All beneficial bacteria % to be tested, % Max Percentage change in flux allowed from one time step to the next

---------

Replicating gut reaction lists with changes in flux on changing model conditions (diet, bacteria): 

Run Model Analysis/Effect_on_SIEC_rxns.m

---------

Replicating gut & brain reaction lists with changes in flux to see the effect of the integrated model (toxin transport): 

1. Copy the file Toxin_ex_fluxes.mat generated from running the Integrated Model (in the folder Results) for the gut model of interest into the folder Model Analysis

2. Run script Model Analysis/Effect_of_oxidative_stress.m 

----------

Replicating bacerial secretion product lists: 

Run Model Analysis/Bacterial_secretion_prods.m

----------

------------------------------------------------------------------------------------------------------------------------------------

MODEL CONSTRUCTION:

1. Individual component construction for the Gut COBRA model: Run scripts in Model construction/Bacterial models, Small intestine model
after ensuring that raw bacterial model xml files, and SIEC .mat file are present in the respective folders. 

2. Merging component models to get Gut-microbiome model including diet input: Run Model construction/Gut & Microbiome model/Comb_merge_host

3. Individual neuronal cell type construction: Run Model Construction/Brain model/Individual/Brain_model.m after ensuring that the raw neuronal xml file is present in this folder. 

4. Merging neuronal cell types to construct representative neuronal cell model: Run Model Construction/Brain model/Combined

------------------------------------------------------------------------------------------------------------------------------------

