clc
clearvars
close all
cde

ben= [ 20;40;60;80 ];

SOX_brain_HF=[0.002215176;0.00217755;0.001937608;0.001491115];

SOX_gut_HF=[0.004497661
0.0044213
0.003925579
0.00300737];

SOX_brain_WD =[0.002532109
0.002318389
0.001940233
0.001507941];

SOX_gut_WD=[0.005149676
0.004708277
0.003930653
0.003041829];

figure(1)
plot(ben,SOX_brain_HF,'b.-')
hold on
plot(ben,SOX_gut_HF,'b.--')
hold on
plot(ben,SOX_brain_WD,'r.-')
hold on
plot(ben,SOX_gut_WD,'r.--')

title('Effect of beneficial bacteria on superoxide')
xlabel('Beneficial bacteria %')
ylabel('Concentration of superoxide (M) ')
legend('Brain- high fiber diet','Gut- high fiber diet','Brain- western diet','Gut- western diet');
print('Effect of beneficial bacteria on SOX','-dpng','-r300')